//
//  VideoChatViewController.h
//  cthedoc
//
//  Created by Miky Braun on 26/07/2016.
//  Copyright © 2016 cthedoc.expert. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoChatViewController;

@interface VideoChatViewController : UIViewController <UIWebViewDelegate>

- (IBAction)buttonStopDidClicked:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backAct;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) NSString* url;


-(IBAction)backToVideo:(UIStoryboardSegue *)segue ;

@end
