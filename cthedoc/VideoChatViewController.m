//
//  VideoChatViewController.m
//  cthedoc
//
//  Created by Miky Braun on 26/07/2016.
//  Copyright © 2016 cthedoc.expert. All rights reserved.
//

#import "VideoChatViewController.h"
#import "StartCallViewController.h"
#import "RTMoviePlayerController.h"
#import "RTMoviePlayerViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import "Recorder.h"
#import "MBProgressHUD.h"
#import "NetworkManager.h"

#define mashovUrl  @"https://piraeus.wizsupport.com/chat/imashov.aspx?op=1&profile=1&info=%22+mashov+%22&lang=he&smc=1"
#define kSnapperChatWebServiceURL @"https://piraeus.wizsupport.com/chat/snapper/snapperview.aspx?room="

#define kTwoMinutes 2*60.0f
#define kVideoChatWebServiceURL @"rtmp://191.235.170.31:1935/wiztest" //@"rtmp://www.wizsupport.com:1935/wiztest"
// @"rtmp://191.235.170.31:1935/wiztest"

//  @yosi -- Change to YES if you want to see logs

#define kShowLogger NO

@interface VideoChatViewController () <RTMoviePlayerViewControllerDelegate, RTMoviePlayerControllerDelegate, StreamingCallbackListener>

// views
@property (weak, nonatomic) IBOutlet UIView *vRepresentiveVideo;
@property (weak, nonatomic) IBOutlet UIView *vMyVideo;

// player
@property (strong, nonatomic) RTMoviePlayerController *streamPlayer;
@property (strong, nonatomic) Recorder *recorder;

@property (assign, nonatomic) BOOL isRecorderStopped;
@property (assign, nonatomic) BOOL isPlayerStopped;
@property (nonatomic,assign) BOOL shouldRestartVideo;

@property (strong, nonatomic) UIAlertController *alertStop;
@property (strong, nonatomic) NSString *log;

// prepare video
@property (strong, nonatomic) NSTimer *getCallContentTimer;
@property (strong, nonatomic) NSTimer *getCallStatusTimer;
@property (assign, nonatomic) BOOL isChatVisible;
@property (assign, nonatomic) BOOL isCallStatusChanged;

@end

@implementation VideoChatViewController
{
    BOOL goToSnapper;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initStopVideoAlert];
    self.isChatVisible = NO;
    self.log = @"";

    [self.backBtn setImage:[UIImage new]];
    [self.backBtn setTitle:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    goToSnapper = NO;
    [self prepareForVideoChat];
    
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self videoChatDidFinish];
    if (self.isChatVisible) {
        [self.recorder endSession];
        [self.streamPlayer stop];
    }
}

#pragma mark - prepare video

-(void) initStopVideoAlert{
    self.alertStop = [UIAlertController alertControllerWithTitle:@"" message:@"האם אתה בטוח שברצונך לסיים את השיחה?" preferredStyle:UIAlertControllerStyleAlert];
    [self.alertStop addAction:[UIAlertAction actionWithTitle:@"כן" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (self.isChatVisible) {
            [self.recorder endSession];
            [self.streamPlayer stop];
        }
        [self.alertStop dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [self.alertStop addAction:[UIAlertAction actionWithTitle:@"ביטול" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self.alertStop dismissViewControllerAnimated:YES completion:nil];
    }]];
}

- (void)prepareForVideoChat
{
    MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.labelText = @"Please Wait...";
    
    [self setGetCallContentTimer];
}


//- (NSString *) getCurrentTimeStamp {
//    return [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
//}

//- (void)startNewChat
//{
//    [[WebService sharedService].data setExtID:[self getCurrentTimeStamp]];
//
//    [[WebService sharedService] webServiceCreateNewChatWithSubject:@"" agent:@"" extra:[WebService sharedService].data.extID
//                                                           success:^(AFHTTPRequestOperation *operation, BOOL created) {
//
//                                                               if (created) {
//                                                                   [self startGetCallStatusTimer];
//                                                               }
//                                                               else {
//                                                                   [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                                                               }
//
//                                                           } failure:^(AFHTTPRequestOperation *operation, Error *error) {
//
//                                                               [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//                                                               UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.title delegate:nil cancelButtonTitle:@"אישור" otherButtonTitles:nil, nil];
//                                                               [alert show];
//
//                                                           }];
//}

//- (void)afterMinuteTimer
//{
//    if (!self.isCallStatusChanged)
//    {
//        [self.getCallStatusTimer invalidate];
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//    }
//}

- (void)afterFourMinutes
{
    if (!self.isChatVisible)
    {
        [self.getCallContentTimer invalidate];
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }
}


//- (void)startGetCallStatusTimer
//{
//    [NSTimer scheduledTimerWithTimeInterval:60.0f target:self selector:@selector(afterMinuteTimer) userInfo:nil repeats:NO];
//    self.getCallStatusTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(getCallStatus) userInfo:nil repeats:YES];
//}
//
//- (void)getCallStatus
//{
//    [[WebService sharedService] webServiceGetCallStatusWithSuccessBlock:^(AFHTTPRequestOperation *operation, WizSupportCallStatus status) {
//
//        if (status == WizSupportCallStatusActive)
//        {
//            [self startGetCallContentTimer];
//        }
//
//    } failure:^(AFHTTPRequestOperation *operation, Error *error) {
//
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.title delegate:nil cancelButtonTitle:@"אישור" otherButtonTitles:nil, nil];
//        [alert show];
//        [self.getCallStatusTimer invalidate];
//        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
//
//    }];
//}

- (void)setGetCallContentTimer
{
    [NSTimer scheduledTimerWithTimeInterval:kTwoMinutes target:self selector:@selector(afterFourMinutes) userInfo:nil repeats:NO];
    self.getCallContentTimer = [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(getCallContent) userInfo:nil repeats:YES];
}

- (void)getCallContent
{
    //    self.isCallStatusChanged = YES;
    
    [[NetworkManager shared] getCallContentWithSuccess:YES success:^(AFHTTPRequestOperation *operation,id data) {
        
        CallContent* callContent = data;
        if (!self.isChatVisible)
        {
            [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
            
            self.isRecorderStopped = NO;
            self.isPlayerStopped = NO;
            [self initStreamer];
            
            self.isChatVisible = YES;
        }
        

        switch (callContent.callStatus) {
            case WizSupportCallStatusActive:
            {
                if(callContent.isVideo<=0){
                    [self performSegueWithIdentifier:@"backToChat" sender:self];
                }
                else{
                    if(callContent.isSnapper>0 && !goToSnapper){
                        goToSnapper = YES;
                       // [self.getCallContentTimer invalidate];
                       // [self performSegueWithIdentifier:@"goToWeb" sender:self];
                        self.webView.hidden = NO;
                        self.url = [kSnapperChatWebServiceURL stringByAppendingString:[NetworkManager shared].data.roomID];
                        [self loadWebChat];

                    }else if (callContent.isSnapper==0)
                    {
                         goToSnapper = NO;
                        _webView.hidden=YES;
                    }
                }
            }
                
                break;
            case WizSupportCallStatusClosed:
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self performSegueWithIdentifier:@"goToWeb" sender:self];
            }
                
                break;
            case WizSupportCallStatusWaiting:
                return ;
                
            default:
                break;
        }
        
    } failure:^(AFHTTPRequestOperation *operation, id data) {
        Error* error = data;
        [self showAlert:error.title];
        [self.getCallContentTimer invalidate];
    }];
    
}


-(void) showAlert:(NSString*) msg {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self.getCallContentTimer invalidate];
        self.isChatVisible = NO;
        [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma mark - start video

-(void)initStreamer
{
    NSString *server = kVideoChatWebServiceURL;
    
    NSString *stream = [NSString stringWithFormat:@"%@l", [NetworkManager shared].data.roomID];
    
    //@"rtmp://www.wizsupport.com:1935/wiztest/yosi"
    
    self.recorder = [[Recorder alloc] initWithServer:[NSString stringWithFormat:@"%@/%@", server, stream]
                                            username:nil
                                            password:nil
                                             preview:self.vMyVideo
                                                mute:NO
                                    callbackListener:self
                                    usingFrontCamera:YES
                                          usingTorch:NO
                                    videoWithQuality:VideoResolution_192x144
                                           audioRate:44100
                                     andVideoBitRate:(51 * MaximumBitrate_192x144)
                                    keyFrameInterval:24
                                     framesPerSecond:25
                                        andShowVideo:YES
                               saveVideoToCameraRoll:NO
                                  autoAdaptToNetwork:YES
                         allowVideoResolutionChanges:YES
                                        bufferLength:kDefaultBufferLength
                                   validOrientations:Orientation_Portrait
                                  previewOrientation:Orientation_Portrait];
    
    [self makeDefaultFadeAnimationWithView:self.vMyVideo andTargetAlphaValue:1.0];
}


- (IBAction)buttonStopDidClicked:(id)sender
{
    
    [self presentViewController:self.alertStop animated:YES completion:nil];
}


- (void)videoChatDidFinish
{
    NSLog(@"videoChatDidFinish - timer stopped.");
    [self.getCallContentTimer invalidate];
    
    self.isChatVisible = NO;
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}


-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


#pragma mark - PLayer Notifications

- (void)playerWillEnterFullscreen:(NSNotification *)notification
{
    NSLog(@"playerWillEnterFullscreen");
}

- (void)playerWillExitFullscreen:(NSNotification *)notification
{
    NSLog(@"playerWillExitFullscreen");
}

- (void)onPlayerViewControllerStateChanged:(DecoderState)state errorCode:(RTError)errCode
{
    NSLog(@"player view controller state changed: %d", state);
}

- (void)onPlayerStateChanged:(DecoderState)state errorCode:(RTError)errCode player:(RTMoviePlayerController *)player
{
    NSLog(@"player state changed: %d", state);
    
    if (state == kDecoderStateStoppedByUser) {
        self.isPlayerStopped = YES;
        [self stop];
    }
}

- (void)stop
{
    if (self.isRecorderStopped && self.isPlayerStopped) {
        
        //        if ([self.delegate respondsToSelector:@selector(videoChatDidFinish:)]) {
        [self videoChatDidFinish];
        //        }
        
//        [self dismissViewControllerAnimated:YES completion:^{
//            //            [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
//        }];
    }
}


#pragma mark -
#pragma mark Recorder Delegate

- (void)clientConnectedFasterThenRepresentive
{
    [self refresh];
    NSLog(@"*********************************************************");
    NSLog(@"Streaming error occurred: Refreshing.......");
    NSLog(@"*********************************************************");
}

- (void)streamingStateChanged:(StreamingState)streamingState withMessage:(NSString *)message
{
    NSLog(@"state changed to %u with message: %@", streamingState, message);
    
    switch (streamingState) {
        case StreamingState_Error:
            self.log = [self.log stringByAppendingFormat:@"Error: %@\n", message];
            break;
        case StreamingState_NoInternet:
            self.log = [self.log stringByAppendingFormat:@"NoInternet: %@\n", message];
            break;
        case StreamingState_Ready:
            self.log = [self.log stringByAppendingFormat:@"Ready: %@\n", message];
            break;
        case StreamingState_Started:
            self.log = [self.log stringByAppendingFormat:@"Started: %@\n", message];
            break;
        case StreamingState_Starting:
            self.log = [self.log stringByAppendingFormat:@"Starting: %@\n", message];
            break;
        case StreamingState_Stopped:
            self.log = [self.log stringByAppendingFormat:@"Stopped: %@\n", message];
            break;
        case StreamingState_Stopping:
            self.log = [self.log stringByAppendingFormat:@"Stopping: %@\n", message];
            break;
        case StreamingState_Warning:
            self.log = [self.log stringByAppendingFormat:@"Warning: %@\n", message];
            break;
            
        default:
            break;
    }
    
    //    self.tvLog.text = self.log;
    
    switch (streamingState) {
        case StreamingState_Stopped:
        {
            if(self.shouldRestartVideo)
            {
                self.shouldRestartVideo = NO;
                [self initStreamer];
                
            }
            else
            {
                self.isRecorderStopped = YES;
                [self stop];
            }
        }
            break;
            
        case StreamingState_Ready:
        {
            [self.recorder start];
        }
            break;
            
        case StreamingState_Started:
        {
            NSString *server = kVideoChatWebServiceURL;
            NSString *stream = [NSString stringWithFormat:@"%@b", [NetworkManager shared].data.roomID];
            //@"rtmp://www.wizsupport.com:1935/wiztest/yosi"
            NSString *string = [NSString stringWithFormat:@"%@/%@_360p", server, stream]; // _160p
            self.streamPlayer = [[RTMoviePlayerController alloc] initWithURLString:string];
            self.streamPlayer.delegate = self;
            self.streamPlayer.view.frame = CGRectMake(0, 0, self.vRepresentiveVideo.frame.size.width, self.vRepresentiveVideo.frame.size.height);
            [self.vRepresentiveVideo addSubview:self.streamPlayer.view];
            self.streamPlayer.contentURLString = string;
            self.streamPlayer.decoderOptions = NULL;
            self.streamPlayer.scalingMode = RTMovieScalingModeAspectFill;
            self.streamPlayer.controlStyle = RTMovieControlStyleStyleNone;
            self.streamPlayer.barTitle = @"";
            self.streamPlayer.statusBarHidden = YES;
            [self.streamPlayer play];
            //            self.btnRefresh.enabled =YES;
            
            [self makeDefaultFadeAnimationWithView:self.vRepresentiveVideo andTargetAlphaValue:1.0];
            
        }
            break;
            
        case StreamingState_Error:
        case StreamingState_NoInternet:
        {
            NSLog(@"*********************************************************");
            NSLog(@"Streaming error occurred: %@", message);
            NSLog(@"*********************************************************");
        }
            break;
            
        default:
            break;
    }
    
    //    StreamingState_Error = 0,
    //    StreamingState_Warning,
    //    StreamingState_Ready,
    //    StreamingState_Starting,
    //    StreamingState_Started,
    //    StreamingState_Stopping,
    //    StreamingState_Stopped,
    //    StreamingState_NoInternet
    
}

- (void)adaptedToNetworkWithState:(AutoAdaptState)adaptState fps:(int)fps bitrate:(int)bitrate resolution:(VideoResolution)resolution
{
    NSLog(@"*********************************************************");
    NSLog(@"new adapt state: %d", adaptState);
    NSLog(@"fps = %d", fps);
    NSLog(@"bitrate = %d", bitrate);
    NSLog(@"*********************************************************");
}



- (void)dismissVc {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)didClickRefresh:(id)sender
{
    [self refresh];
}

- (void)refresh
{
    //    self.btnRefresh.enabled = NO;
    self.shouldRestartVideo = YES;
    [self.streamPlayer stop];
    if (self.recorder.active)
    {
        [self.recorder endSession];
        [self.recorder stop];
    }
}

-(IBAction)backToVideo:(UIStoryboardSegue *)segue {
    
}

// UI - Utils
- (void)makeDefaultFadeAnimationWithView:(UIView *)view andTargetAlphaValue:(float)alpha {
    
    [UIView animateWithDuration:0.25f animations:^{
        [view setAlpha:alpha];
    } completion:^(BOOL finished) {
        
    }];
    
}

#pragma New webview code 
-(void) loadWebChat {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString* webStringURL = [self.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:webStringURL];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}


@end
