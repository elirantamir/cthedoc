//
//  ChatViewController.h
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessagesComposerTextView.h"
#import "ChatMessageViewController.h"



@interface ChatViewController : UIViewController<UIScrollViewDelegate,UITextViewDelegate>

@property NSArray* messages;

@property (weak, nonatomic) IBOutlet JSQMessagesComposerTextView *messageTextView;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScroll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *toolbarBottomLayout;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;

- (IBAction)sendMsg:(id)sender;

- (IBAction)backAct:(id)sender;

-(IBAction)backToChat:(UIStoryboardSegue *)segue ;

@end
