
#import <UIKit/UIKit.h>

@class DecodeManager;
@class VideoFrame;

/**
 
 *  Uses opengl framework to render pictures & make color conversion fastly. GLES2View is a subclass of UIView and its opengl settings are ready for opengl rendering.
 */
@interface GLES2View : UIView

#pragma mark - public methods

/**
*  Initialize openGL view with DecodeManager
*
*  @param decoder DecodeManager object to be feed from
*
*  @return 0 for succes and non-zero for failure
*/
- (int)initGLWithDecodeManager:(DecodeManager *)decoder;

/**
 *  Enable-disable retina frames if device has retina support, default is YES
 *
 *  @param value Specify YES for enabling or NO for disabling Retina
 */
- (void)enableRetina:(BOOL)value;

/** 
 * Get snapshot of glview in UIImage format
 *
 * @return UIImage object
 */
- (UIImage *)snapshot;

/**
 *  update the openGL screen with new frame
 *
 *  @param vidFrame needs VideoFrame object to draw screen
 */
- (void)updateScreenWithFrame:(VideoFrame *)vidFrame;

/**
 *  destroy openGL view
 */
- (void)shutdown;

@end
