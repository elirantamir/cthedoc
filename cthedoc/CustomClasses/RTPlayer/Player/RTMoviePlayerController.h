
#import <UIKit/UIKit.h>
#import "AVDecodeManager.h"

/**
 * Determines the User interface of control elements on screen
 *
 * PlayerControlStyle enums are
 * - kVideoStreamPlayerControlStyleNone        > Shows only Video screen, no bar, no panel, no any user interface component
 * - kVideoStreamPlayerControlStyleEmbedded    > Shows small User interface elements on screen
 * - kVideoStreamPlayerControlStyleFullScreen  > Shows full width bar and a big control panel on screen
 */

typedef enum {
    RTMovieControlStyleStyleNone,
    RTMovieControlStyleEmbedded,
    RTMovieControlStyleFullscreen
} RTMovieControlStyle;

enum {
    RTMovieMediaTypeMaskNone  = 0,
    RTMovieMediaTypeMaskVideo = 1 << 0,
    RTMovieMediaTypeMaskAudio = 1 << 1
};
typedef NSInteger RTMovieMediaTypeMask;

typedef enum {
    RTMovieScalingModeAspectFit,
    RTMovieScalingModeAspectFill,
    RTMovieScalingModeFill
} RTMovieScalingMode;

/* Player Fullscreen mode changed notifications */

///Description of notification which is posted when Player will enter fullscreen
extern NSString *RTPlayerWillEnterFullscreenNotification;

///Description of notification which is posted when Player did enter fullscreen
extern NSString *RTPlayerDidEnterFullscreenNotification;

///Description of notification which is posted when Player will exit fullscreen
extern NSString *RTPlayerWillExitFullscreenNotification;

///Description of notification which is posted when Player did exit fullscreen
extern NSString *RTPlayerDidExitFullscreenNotification;


/* Decoder decode option keys */

///Defining RTSP protocol transport layer. Values are predefined under "Decoder decode option values"
extern NSString *DECODER_OPT_KEY_RTSP_TRANSPORT;

///Selection of audio default stream by index. Value must be an NSNumber object. (High priority)
extern NSString *DECODER_OPT_KEY_AUD_STRM_DEF_IDX;

///Selection of audio default stream by string. Value must be an NSString object (normal priority)
extern NSString *DECODER_OPT_KEY_AUD_STRM_DEF_STR;

///FFmpeg can not determine some formats, so we force ffmpeg to use mjpeg format. Value must be @"1" which is an NSString object
extern NSString *DECODER_OPT_KEY_FORCE_MJPEG;

///FFmpeg has many server configuration parameters and if this key and value (value must be always @"1") is set, all parameter will pass through ffmpeg without any modification. This is like playing a url with fflay, for example, ffplay rtsp://xxx.xxx.xxx.xxx -rtsp_transport tcp
extern NSString *DECODER_OPT_KEY_PASS_THROUGH;

/* Decoder decode option values*/

///RTSP uses UDP transport layer - advantage fast, disadvantage packets can be lost
extern NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP;

///RTSP uses TCP transport layer, advantage no packet loss, disadvantage slow when comparing with UDP
extern NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_TCP;

// /RTSP uses multicast UDP to retrieve packets
extern NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP_MULTICAST;

///RTSP uses http tunnelling to retrieve packets
extern NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_HTTP;

@class RTMoviePlayerController;

/**
 *  Implement this delegate if you want to get notified about state changes with error codes of PlayerController
 */
@protocol RTMoviePlayerControllerDelegate <NSObject>

@optional
/**
 *  Optional delegate method, add this method to your viewcontroller if you want to be notified
 *
 *  @param state   Indicates the state in DecoderState type
 *  @param errCode Indicates the error code in RTError type
 *  @param player  The current player
 */
- (void)onPlayerStateChanged:(DecoderState)state errorCode:(RTError)errCode player:(RTMoviePlayerController*)player;
- (void)clientConnectedFasterThenRepresentive;

@end

@class GLES2View;

/**
 * A movie player that manages the playback of a movie from a local file, but especially from a network stream (RTMP, RTSP, HTTP, MMS). Copying the functionality of MPMoviePlayerController, the playback occurs in a view owned by the movie player and takes place either fullscreen or inline. It is not based on a single instance low-level player (like MPMoviePlayerCOntroller, using a single instance of AVPlayer) therefore it can be used in multiple instances at the same time.
 */

@interface RTMoviePlayerController : NSObject

/// The location of the file or remote stream url in string format. If it's a file then it must be located either in your app directory or on a remote server
@property (nonatomic, retain) NSString *contentURLString;

/// Streaming options according to the used protocol
@property (nonatomic, retain) NSDictionary *decoderOptions;

/// Indicates the decoder states in DecoderState enumerations
@property (nonatomic, readonly) DecoderState decoderState;

/// The bar title of Video Player
@property (nonatomic, retain) NSString *barTitle;

/// Set your Parent View Controller as delegate If you want to be notified for state changes
@property (nonatomic, assign) id<RTMoviePlayerControllerDelegate> delegate;

/// Specify YES to hide status bar, default is NO. Effective only in fullscreen presenting
@property (nonatomic, assign, getter=isStatusBarHidden) BOOL statusBarHidden;

/// The view containing the movie content and controls. Any custom views must be added on this
@property (nonatomic, retain, readonly) UIView *view;

/// The video rendering view, opengl configuration is done to be ready for rendering
@property (nonatomic, readonly) GLES2View *renderView;

/// The background color of player, default is black
@property (nonatomic, retain) UIColor *backgroundColor;

/// An empty and transparent UIViewController used for fullscreen - embedded transitioning. Do not set it If you have a very powerful reason to do that
@property (nonatomic, assign) UIViewController *containerVc;

/// Indicates the state of player presenting mode. If set, then this makes embedded player fullscreen or makes fullscreen player embedded
@property (nonatomic, assign, getter=isFullscreen) BOOL fullscreen;

/// Determines the User interface of control elements on screen
@property (nonatomic, assign) RTMovieControlStyle controlStyle;

/// The time, specified in seconds within the video timeline, when playback should start
@property (nonatomic, assign) int64_t initialPlaybackTime;

/// Repeats movie playback <number> times. 0 means forever
@property (nonatomic, assign) int repeatTimes;

/// Stop player when video is done playing
@property (nonatomic, assign) BOOL autoStopAtEnd;

/// Specify YES to show video in extended screen, default is No
@property (nonatomic, assign) BOOL allowsAirPlay;

/// Indicates whether the movie player is currently playing video via AirPlay
@property (nonatomic, readonly, getter=isAirPlayVideoActive) BOOL airPlayVideoActive;

/// The duration of the movie, measured in seconds. IMPORTANT: this is available only for local and remote FILES, and not for live streams
@property (nonatomic, readonly) float duration;

/// The current position in the track measured in seconds
@property (nonatomic, readonly) float currentPosition;

/// The types of media available in the movie (read-only). You can OR the specified constants together to specify a movie
@property (nonatomic, readonly) RTMovieMediaTypeMask movieMediaTypes;

/// The width and height of the movie frame (read-only)
@property (nonatomic, readonly) CGSize naturalSize;

/// The scaling mode to use when displaying the movie. Changing this property while the movie player is visible causes the current movie to animate to the new scaling mode.
@property (nonatomic, assign) RTMovieScalingMode scalingMode;

/**
 *  Initialization of NStreamMoviePlayerController object with the url string object
 *
 *  @param urlString The location of the movie file. This file must be located either in your app directory or on a remote server.
 *
 *  @return NStreamMoviePlayerController object initialized with the movie at the specified URL.
 */
- (id)initWithURLString:(NSString *)urlString;


#pragma mark Player public control methods

/**
 *  This method plays the stream/file if urlstring is given in the initialization or content url is set
 */
- (void)play;

/**
 *  Toggle play or pause the stream/file
 */
- (void)togglePause;

/**
 *  This method stops the stream/file
 */
- (void)stop;

/**
 *  Frames are drawn on view, so scale method switches view contentmode property from UIViewContentModeScaleAspectFit to UIViewContentModeScaleAspectFill or the opposite
 */
- (void)scale;

/**
 *  Update the screen with the following video frame
 */
- (void)stepToNextFrame;

/**
 *  Set current playing postition in files both for located locally or located remotely
 *
 *  @param position A double value in seconds
 */
- (void)setVideoCurrentPosition:(float)position;

/**
 *  Cycle/Change to next the audio stream if remote stream/file has more than 1 audio stream
 */
- (void)changeAudioStream;

/**
 * Mute audio
 *
 * This does not have any effect on MPVolumeView or not have any relation with MPVolumeView
 * 
 * @param value If YES, audio will be muted
 */
- (void)setMute:(BOOL)value;

/** Get snapshot of glview in UIImage format
 
 Sample code to show how to save the snapshot
 
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *savedImagePath = [documentsDirectory stringByAppendingPathComponent:@"savedImage.png"];
        UIImage *image = [self snapshot];
        NSData *imageData = UIImagePNGRepresentation(image);
        [imageData writeToFile:savedImagePath atomically:NO];
 
 @return UIImage object
 */
- (UIImage *)snapshot;

#pragma mark Player UI methods

/**
 *  Causes the movie player to enter or exit full-screen mode
 *
 *  @param fullscreen Specify YES to enter full-screen mode or NO to exit full-screen mode
 *  @param animated Specify YES to animate the transition between modes or NO to switch immediately to the new mode
 */
- (void)setFullScreen:(BOOL)fullscreen animated:(BOOL)animated;

- (float)speakerVolume;

@end

