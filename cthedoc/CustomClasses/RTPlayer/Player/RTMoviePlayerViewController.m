
#import "RTMoviePlayerViewController.h"
#import "GLES2View.h"

#import <MediaPlayer/MediaPlayer.h>


@interface RTMoviePlayerViewController ()<RTMoviePlayerControllerDelegate> {
    NSString *_urlString;
    NSDictionary *_options;
    RTMoviePlayerController *_playerController;
    BOOL _allowAirPlay;
}

@end

@implementation RTMoviePlayerViewController

@synthesize barTitle = _barTitle;
@synthesize statusBarHidden = _statusBarHidden;
@synthesize delegate = _delegate;
@synthesize allowAirPlay = _allowAirPlay;

- (id)initWithURLString:(NSString *)urlString decoderOptions:(NSDictionary *)options {

    self = [super init];
    if (self) {
        // Custom initialization
        _urlString = [urlString retain];
        _options = [options retain];
        _playerController = [[RTMoviePlayerController alloc] initWithURLString:_urlString];
        _playerController.decoderOptions = _options;
        _playerController.delegate = self;
        return self;
    }
    return nil;
}

#pragma mark View life cycle


- (void)loadView
{
    CGRect bounds = [[UIScreen mainScreen] applicationFrame];

    if (UIInterfaceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
        bounds =  CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.height, bounds.size.width);
    }

    self.view = [[[UIView alloc] initWithFrame:bounds] autorelease];
    self.view.backgroundColor = [UIColor blackColor];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
    if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
        //running on iOS 7.0 or higher
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
#endif

    _playerController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    _playerController.view.frame = self.view.bounds;
    _playerController.containerVc = self;
    [_playerController setFullscreen:YES];
    [self.view addSubview:_playerController.view];
    [_playerController play];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

#pragma mark - Public methods

- (void)setBarTitle:(NSString *)barTitle {
    [_barTitle release];
    _barTitle = nil;
    _barTitle = [barTitle retain];
    _playerController.barTitle = _barTitle;
}

- (void)setStatusBarHidden:(BOOL)statusBarHidden {
    _statusBarHidden = statusBarHidden;
    _playerController.statusBarHidden = _statusBarHidden;
}

- (void)setDelegate:(id<RTMoviePlayerViewControllerDelegate>)delegate {
    _delegate = delegate;
}

- (void)setAllowAirPlay:(BOOL)allowAirPlay {
    _allowAirPlay = allowAirPlay;
    _playerController.allowsAirPlay = _allowAirPlay;
}

#pragma mark View controller rotation methods & callbacks

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
- (BOOL)shouldAutorotate {
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (BOOL)prefersStatusBarHidden
{
    return _playerController.statusBarHidden;
}

#endif

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIDeviceOrientationPortraitUpsideDown);
}

#pragma mark - PlayerController callback

- (void)onPlayerStateChanged:(DecoderState)state errorCode:(RTError)errCode player:(RTMoviePlayerController *)player{
    if(_delegate && [_delegate respondsToSelector:@selector(onPlayerViewControllerStateChanged:errorCode:player:)]) {
        [_delegate onPlayerViewControllerStateChanged:state errorCode:errCode player: player];
    }
}

#pragma mark - Memory events & deallocation

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [super viewDidUnload];
}

- (void)dealloc {
    [_urlString release];
    [_options release];
    [_playerController release];
    [super dealloc];
}

- (void)setVolume:(float)volume
{
    //[_playerController setVolume: volume];
}

@end