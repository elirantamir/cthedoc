
#import "VideoFrame.h"

@implementation ColorPlane

- (id)init {
    self = [super init];
    return self;
}

- (void)dealloc {
    if (_data && _size) {
        free(_data);
        _data = NULL;
    }
    [super dealloc];
}

@end

@interface VideoFrame () {
    VideoStreamColorFormat _colorFormat;
}

@end


@implementation VideoFrame

@synthesize width, height, pts;
@synthesize pos, serial, aspectRatio;

@synthesize pRGB = _pRGB;

@synthesize pLuma    = _pLuma;
@synthesize pChromaB = _pChromaB;
@synthesize pChromaR = _pChromaR;

- (id) initWithColorFormat:(VideoStreamColorFormat) format {
    self = [super init];
    if (self) {
        _colorFormat = format;
        
        if (format == VideoStreamColorFormatYUV) {
            _pLuma = [[ColorPlane alloc] init];
            _pChromaB = [[ColorPlane alloc] init];
            _pChromaR = [[ColorPlane alloc] init];
        } else {
            _pRGB = [[ColorPlane alloc] init];
        }
    }
    
    return self;
}

- (void) dealloc {
    
    if (_colorFormat == VideoStreamColorFormatYUV) {
        [_pLuma release];
        [_pChromaB release];
        [_pChromaR release];
    } else {
        [_pRGB release];
    }
    [super dealloc];
}

@end
