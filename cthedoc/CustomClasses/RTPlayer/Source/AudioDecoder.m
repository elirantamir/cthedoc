
#import "AudioDecoder.h"
#import "Packet.h"

#include "libavutil/time.h"

//defines
/* we use about AUDIO_DIFF_AVG_NB A-V differences to make the average */
#define AUDIO_DIFF_AVG_NB   20

#define OUTPUT_BUS              0
#define MAX_AU_BUFFER_SIZE      4096
#define ERROR_HERE(status) do {if (status) fprintf(stderr, "ERROR %d [%s:%u]\n", (int)status, __func__, __LINE__);}while(0);

//Audio structs
typedef struct AudioParams {
    int freq;
    int channels;
    int64_t channelLayout;
    enum AVSampleFormat fmt;
} AudioParams;


//Audio functions
static OSStatus audioUnitCallback(void *inRefCon, AudioUnitRenderActionFlags  *ioActionFlags,
                  const AudioTimeStamp        *inTimeStamp, UInt32                       inBusNumber,
                  UInt32                       inNumberFrames, AudioBufferList             *ioData);

@interface AudioDecoder() {
    NSMutableData *_rawData; /* raw audio data */
    AVFrame *_frame; /* audio frame */
    AVPacket _packetTmp; /* temp audio packet */
    
    //AV syncing
    int64_t _callbackTime;
    double _clock;
    int _hwBufSize;
    unsigned int _bufSize; /* in bytes */
    unsigned int _bufIndex; /* in bytes */
    int _writeBufSize;
    double _currentPts;
    double _currentPtsDrift;
    
    int _clockSerial;
    double _diffCum; /* used for AV difference average computation */
    double _diffAvgCoef;
    double _diffThreshold;
    int _diffAvgCount;

    //AU
    AudioComponentInstance _audioUnit;

    //Audio resampling
    struct AudioParams _sourceParams;
    struct AudioParams _targetParams;
    struct SwrContext *_swrCtx;
    uint8_t *_swrBufferTemp;
    unsigned int _sizeSwrBufferTemp; /* in bytes */

    int64_t _currentPos;
    BOOL _eof;
    float _volumeLevel;
}

@end

@implementation AudioDecoder

@synthesize isWaitingForPackets = _waitingForPackets;
@synthesize currentPtsDrift = _currentPtsDrift;
@synthesize currentPos = _currentPos;
@synthesize volumeLevel = _volumeLevel;

- (id)initWithCodecContext:(AVCodecContext*)cdcCtx stream:(AVStream *)strm streamId:(NSInteger)sId manager:(id)manager {
    self = [super initWithCodecContext:cdcCtx stream:strm streamId:sId manager:manager];
    if (self) {
        _rawData = [[NSMutableData alloc] init];
        _frame = avcodec_alloc_frame();
        memset(&_packetTmp, 0, sizeof(_packetTmp));
        
        if (!_frame) {
            return nil;
        }
        [self initValues];
    }
    return self;
}

- (void)initValues {
    _hwBufSize = MAX_AU_BUFFER_SIZE;
    _bufSize = 0;
    _bufIndex = 0;
    _waitingForPackets = NO;
    _sourceParams.fmt = AV_SAMPLE_FMT_S16;
    _sourceParams.channelLayout = av_get_default_channel_layout(_codecContext->channels);
    _sourceParams.channels = _codecContext->channels;
    _sourceParams.freq = _codecContext->sample_rate;
    _targetParams = _sourceParams;

    _currentPts = -av_gettime() / 1000000.0;

    _clockSerial = -1;
    /* init averaging filter */
    _diffAvgCoef  = exp(log(0.01) / AUDIO_DIFF_AVG_NB);
    _diffAvgCount = 0;
    /* since we do not have a precise anough audio fifo fullness,
     we correct audio sync only if larger than this threshold */
    _diffThreshold = 2.0 * _hwBufSize / av_samples_get_buffer_size(NULL, _targetParams.channels, _targetParams.freq, _targetParams.fmt, 1);

    _eof = NO;
    _volumeLevel = 1.0;
}

#pragma mark AV syncing

- (double)audioClock {

    if (_clockSerial != _queueSerial)
        return NAN;
    if ([(DecodeManager *)_manager streamIsPaused]) {
        return _currentPts;
    }
    return _currentPtsDrift + (av_gettime() / 1000000.0);
}

- (int)syncAudio:(int)nb_samples
{
    int wantedNumberOfSamples = nb_samples;

    /* if not master, then we try to remove or add samples to correct the clock */

    /*if (masterSyncType(is) != AV_SYNC_AUDIO_MASTER) {
        double diff, avg_diff;
        int min_nb_samples, max_nb_samples;

        diff = get_audio_clock(is) - get_master_clock(is);

        if (!isnan(diff) && fabs(diff) < AV_NOSYNC_THRESHOLD) {
            is->_diffCum = diff + is->_diffAvgCoef * is->_diffCum;
            if (is->_diffAvgCount < AUDIO_DIFF_AVG_NB) {
                // not enough measures to have a correct estimate
                is->_diffAvgCount++;
            } else {
                //estimate the A-V difference
                avg_diff = is->_diffCum * (1.0 - is->_diffAvgCoef);

                if (fabs(avg_diff) >= is->_diffThreshold) {
                    wantedNumberOfSamples = nb_samples + (int)(diff * is->audio_src.freq);
                    min_nb_samples = ((nb_samples * (100 - SAMPLE_CORRECTION_PERCENT_MAX) / 100));
                    max_nb_samples = ((nb_samples * (100 + SAMPLE_CORRECTION_PERCENT_MAX) / 100));
                    wantedNumberOfSamples = FFMIN(FFMAX(wantedNumberOfSamples, min_nb_samples), max_nb_samples);
                }
                av_dlog(NULL, "diff=%f adiff=%f sample_diff=%d apts=%0.3f %f\n",
                        diff, avg_diff, wantedNumberOfSamples - nb_samples,
                        is->audio_clock, is->_diffThreshold);
            }
        } else {
            //too big difference : may be initial PTS errors, so reset A-V filter
            is->_diffAvgCount = 0;
            is->_diffCum       = 0;
        }
    }*/
    
    return wantedNumberOfSamples;
}

#pragma mark - On State change actions

- (void)setEOF:(BOOL)value {

    _eof = value;

    if (_eof) {
        _waitingForPackets = NO;
        pthread_mutex_lock(&_mutexPkt);
        pthread_cond_signal(&_condPkt);
        pthread_mutex_unlock(&_mutexPkt);
    }
}

#pragma mark - Audio Unit

- (AudioComponentInstance)audioUnit {
    return _audioUnit;
}

- (void)startUnit {
    if (_audioUnit) {
        OSStatus status = noErr;
        status = AudioOutputUnitStart(_audioUnit);
        ERROR_HERE(status);
    }
}

- (void)stopUnit {
    if (_audioUnit) {
        OSStatus status = noErr;
        status = AudioOutputUnitStop(_audioUnit);
        ERROR_HERE(status);
    }
}

- (void)startAudioSystem {

	OSStatus status = noErr;

	AudioComponentDescription desc;
	desc.componentType          = kAudioUnitType_Output;
	desc.componentSubType       = kAudioUnitSubType_RemoteIO;
	desc.componentFlags         = 0;
	desc.componentFlagsMask     = 0;
	desc.componentManufacturer  = kAudioUnitManufacturer_Apple;

	AudioComponent outputComponent = AudioComponentFindNext(NULL, &desc);

	status = AudioComponentInstanceNew(outputComponent, &_audioUnit);
    ERROR_HERE(status);

	UInt32 flag = 1;
	status = AudioUnitSetProperty(_audioUnit, kAudioOutputUnitProperty_EnableIO, kAudioUnitScope_Output, OUTPUT_BUS, &flag, sizeof(flag));
    ERROR_HERE(status);


	AudioStreamBasicDescription audioFormat;
    audioFormat.mFormatID         = kAudioFormatLinearPCM;
    audioFormat.mSampleRate       = _sourceParams.freq;
    audioFormat.mChannelsPerFrame = _sourceParams.channels;
    audioFormat.mBitsPerChannel   = 16;
    audioFormat.mFramesPerPacket  = 1;
    audioFormat.mBytesPerFrame    = audioFormat.mChannelsPerFrame * audioFormat.mBitsPerChannel/8;
    audioFormat.mBytesPerPacket   = audioFormat.mBytesPerFrame * audioFormat.mFramesPerPacket;
    audioFormat.mFormatFlags      = kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked;

	status = AudioUnitSetProperty(_audioUnit, kAudioUnitProperty_StreamFormat, kAudioUnitScope_Input, OUTPUT_BUS, &audioFormat, sizeof(audioFormat));
    ERROR_HERE(status);

	AURenderCallbackStruct callbackStruct;
	callbackStruct.inputProc       = audioUnitCallback;
	callbackStruct.inputProcRefCon = self;

    if (!status)
    {
        status = AudioUnitSetProperty(_audioUnit, kAudioUnitProperty_SetRenderCallback, kAudioUnitScope_Input, OUTPUT_BUS, &callbackStruct, sizeof(callbackStruct));
        ERROR_HERE(status);
    }
    if (!status)
    {
        status = AudioUnitInitialize(_audioUnit);
        ERROR_HERE(status);
    }

    [self startUnit];
}

#pragma mark AU stop

- (void)stopAudioSystem
{
    if (_audioUnit) {
        OSStatus err;
        [self stopUnit];

        err = AudioUnitUninitialize(_audioUnit);
        if (err) ERROR_HERE(err);

        err = AudioComponentInstanceDispose(_audioUnit);
        if (err) ERROR_HERE(err);
    }
}

#pragma mark AU Callback

- (OSStatus)audioUnitCallback:(AudioUnitRenderActionFlags *)ioActionFlags
                     timestamp:(const AudioTimeStamp       *)inTimeStamp
                     busNumber:(UInt32                      )inBusNumber
                  numberFrames:(UInt32                      )inNumberFrames
                          data:(AudioBufferList            *)ioData
{
    uint16_t *buffer = (uint16_t *)ioData->mBuffers[0].mData;
    int wantedNumberOfSamples;
    BOOL newPktRequired = NO;
    int pktSerial = 0;

start:
    _callbackTime = av_gettime();

    int size, sizeOfPktProcessed;
    int64_t decChannelLayout;
    int lenSwr;

    Packet *pkt;
    int countPkt = 0;
    pkt = nil;

    if (!_abortRequest) {
        pthread_mutex_lock(&_mutexPkt);
        countPkt = (int)[_pktQueue count];
        if (!countPkt && !_eof) {
            _waitingForPackets = YES;
            pthread_cond_wait(&_condPkt, &_mutexPkt);
            _callbackTime = av_gettime();
            if (_abortRequest) {
                pthread_mutex_unlock(&_mutexPkt);
                return noErr;
            }
        }
        _waitingForPackets = NO;

        pkt = (Packet *)[_pktQueue lastObject];

        if (pkt && (pkt.serial != _queueSerial)) {
            /* the serial is updated (audio stream may be changed or seek process is done),
             drop packet to get a new one */
            _pktQueueSize = _pktQueueSize - pkt.size;
            [_pktQueue removeLastObject];
            newPktRequired = YES;
        }

        if (pkt && (pkt.flush)) {
            _pktQueueSize = _pktQueueSize - pkt.size;
            [_pktQueue removeLastObject];
            avcodec_flush_buffers(_codecContext);
            newPktRequired = YES;
        }
        pthread_mutex_unlock(&_mutexPkt);
    }

    if (newPktRequired) {
        newPktRequired = NO;
        goto start;
    }

    int sizeMData = ioData->mBuffers[0].mDataByteSize;
    int maxBytes = sizeMData;

    _packetTmp.data = pkt ? (uint8_t *)pkt.samples : NULL;
    _packetTmp.size = pkt.size;
    _currentPos = pkt.pts;
    pktSerial = pkt.serial;

    if (!_abortRequest && !_packetTmp.data) {
        goto fail;
    }

    int dataSize;
    int gotFrame;

    int bytesPerSec = 0;

    if ([(DecodeManager *)_manager streamIsPaused] ||
        _abortRequest) {
        _bufSize = maxBytes;
        _bufIndex = 0;
        for (UInt32 i=0; i < ioData->mNumberBuffers; i++){
            memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);
        }
        *ioActionFlags |= kAudioUnitRenderAction_OutputIsSilence;

        goto success;
    }

    if ([_rawData length] < maxBytes) {

        if (pkt.pts != AV_NOPTS_VALUE) {
            _clock = av_q2d(_stream->time_base)*pkt.pts;
            _clockSerial = pkt.serial;
        }

        int pktSize = _packetTmp.size;

        while (_packetTmp.size > 0) {

            uint8_t *framePtr;
            
            avcodec_get_frame_defaults(_frame);
            sizeOfPktProcessed = avcodec_decode_audio4(_codecContext, _frame, &gotFrame, &_packetTmp);
            if (sizeOfPktProcessed < 0) {
                goto fail;
            }

            _packetTmp.data += sizeOfPktProcessed;
            _packetTmp.size -= sizeOfPktProcessed;

            size = av_samples_get_buffer_size(NULL, _codecContext->channels,
                                              _frame->nb_samples,
                                              _codecContext->sample_fmt, 1);


            decChannelLayout = (_frame->channel_layout && av_frame_get_channels(_frame) == av_get_channel_layout_nb_channels(_frame->channel_layout)) ?
            _frame->channel_layout : av_get_default_channel_layout(av_frame_get_channels(_frame));
            //wantedNumberOfSamples = synchronize_audio(is, is->frame->nb_samples);
            wantedNumberOfSamples = [self syncAudio:_frame->nb_samples];

            if (_frame->format           != _sourceParams.fmt ||
                decChannelLayout         != _sourceParams.channelLayout ||
                _frame->sample_rate      != _sourceParams.freq          ||
                (wantedNumberOfSamples       != _frame->nb_samples && !_swrCtx)) {
                swr_free(&_swrCtx);
                _swrCtx = swr_alloc_set_opts(NULL, _targetParams.channelLayout, _targetParams.fmt, _targetParams.freq, decChannelLayout, _frame->format, _frame->sample_rate, 0, NULL);
                if (!_swrCtx || swr_init(_swrCtx) < 0) {
                    SLog(kLogLevelDecoderExtra, @"Cannot create sample rate converter for conversion of %d Hz %s %d channels to %d Hz %s %d channels!\n",
                          _frame->sample_rate, av_get_sample_fmt_name(_frame->format), av_frame_get_channels(_frame),
                          _targetParams.freq, av_get_sample_fmt_name(_targetParams.fmt), _targetParams.channels);
                    goto fail;
                }
                _sourceParams.channelLayout = decChannelLayout;
                _sourceParams.channels = av_frame_get_channels(_frame);
                _sourceParams.freq = _frame->sample_rate;
                _sourceParams.fmt = _frame->format;
            }


            if (_swrCtx) {
                const uint8_t **in = (const uint8_t **)_frame->extended_data;
                uint8_t **out = &_swrBufferTemp;
                int out_count = wantedNumberOfSamples * _targetParams.freq / _frame->sample_rate + 256;
                int out_size  = av_samples_get_buffer_size(NULL, _targetParams.channels, out_count, _targetParams.fmt, 0);
                if (wantedNumberOfSamples != _frame->nb_samples) {
                    if (swr_set_compensation(_swrCtx, (wantedNumberOfSamples - _frame->nb_samples) * _targetParams.freq / _frame->sample_rate,
                                             wantedNumberOfSamples * _targetParams.freq / _frame->sample_rate) < 0) {
                        fprintf(stderr, "swr_set_compensation() failed\n");
                        goto fail;
                    }
                }
                av_fast_malloc(&_swrBufferTemp, &_sizeSwrBufferTemp, out_size);
                if (!_swrBufferTemp) {
                    goto fail;
                }
                
                lenSwr = swr_convert(_swrCtx, out, out_count, in, _frame->nb_samples);
                if (lenSwr < 0) {
                    fprintf(stderr, "swr_convert() failed\n");
                    goto fail;
                }
                if (lenSwr == out_count) {
                    fprintf(stderr, "warning: audio buffer is probably too small\n");
                    swr_init(_swrCtx);
                }
                framePtr = _swrBufferTemp;
                _bufSize = lenSwr * _targetParams.channels * av_get_bytes_per_sample(_targetParams.fmt);
            } else {
                framePtr = _frame->data[0];
                _bufSize = size;
            }
            
            _bufIndex = 0;

            if (size < 0) {
                goto fail;
            }
            
            _clock += (double)size /
            (_codecContext->channels * _codecContext->sample_rate * av_get_bytes_per_sample(_codecContext->sample_fmt));

            if (_volumeLevel != 1.0) {
                for (int i = 0; i < _bufSize; i++) {
                    framePtr[i] = framePtr[i] * _volumeLevel;
                }
            }
            [_rawData appendBytes: (int16_t *)framePtr length:_bufSize];
        }

        pthread_mutex_lock(&_mutexPkt);
        if (pktSerial == _queueSerial) {
            _pktQueueSize = _pktQueueSize - pktSize;
            [_pktQueue removeLastObject];
        }
        pthread_mutex_unlock(&_mutexPkt);
    }

    NSUInteger dataLength = [_rawData length];

    if(dataLength >= maxBytes) {
        dataSize = maxBytes;
    } else {
        goto start;
    }

    _bufIndex += dataSize;
    memcpy(buffer, (uint16_t *)[_rawData mutableBytes], dataSize);
    [_rawData replaceBytesInRange:NSMakeRange(0, dataSize) withBytes:NULL length:0];

    goto success;

fail:

    pthread_mutex_lock(&_mutexPkt);
    if ([_pktQueue count]) {
        _pktQueueSize = _pktQueueSize - _packetTmp.size;
        [_pktQueue removeLastObject];
    }
    pthread_mutex_unlock(&_mutexPkt);


    _bufSize = maxBytes;
    _bufIndex = 0;
    for (UInt32 i=0; i < ioData->mNumberBuffers; i++){
        memset(ioData->mBuffers[i].mData, 0, ioData->mBuffers[i].mDataByteSize);
    }

success:

    bytesPerSec = _targetParams.freq * _targetParams.channels * av_get_bytes_per_sample(_targetParams.fmt);
    _writeBufSize = _bufSize - _bufIndex;
    _currentPts = _clock - (double)(2 * _hwBufSize + _writeBufSize) / bytesPerSec;
    _currentPtsDrift = _currentPts - _callbackTime / 1000000.0;
    if (_clockSerial == _queueSerial)
        [(DecodeManager *)_manager checkExternalClockSync:_currentPts];

    return noErr;
}

OSStatus
audioUnitCallback(void                        *inRefCon,
                  AudioUnitRenderActionFlags  *ioActionFlags,
                  const AudioTimeStamp        *inTimeStamp,
                  UInt32                       inBusNumber,
                  UInt32                       inNumberFrames,
                  AudioBufferList             *ioData)
{
    AudioDecoder *decoder = (AudioDecoder*)inRefCon;
    return [decoder audioUnitCallback:ioActionFlags
                       timestamp:inTimeStamp
                       busNumber:inBusNumber
                    numberFrames:inNumberFrames
                            data:ioData];
}


#pragma mark - Shutdown

- (void)shutdown {

    [self unlockQueues];

    if (_streamId >= 0) {
		_stream->discard = AVDISCARD_ALL;
	}

    [self stopAudioSystem];

    [[NSNotificationCenter defaultCenter] removeObserver:self];

    if (_swrCtx)
        swr_free(&_swrCtx);

    if (_swrBufferTemp) {
        av_freep(&_swrBufferTemp);
        _swrBufferTemp = 0;
    }

    if (_codecContext) {
        avcodec_close(_codecContext);
    }
    if (_frame) {
        av_free(_frame);
    }
    [self clearPktQueue];
}

- (void)unlockQueues {    
    [super unlockQueues];
}

- (void)dealloc {
    SLog(kLogLevelDecoderExtra, @"Audio Decoder is deallocated...");
    [_rawData release];
    [super dealloc];
}

@end
