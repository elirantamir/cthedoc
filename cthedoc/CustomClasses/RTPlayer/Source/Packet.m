
#import "Packet.h"

@interface Packet (){
    int _size;
    int16_t* _samples;
    double _pts;
    double _dts;
    int64_t _pos;
    int _serial;
    BOOL _flush;
}

@end

@implementation Packet

@synthesize size = _size;
@synthesize samples = _samples;
@synthesize pts = _pts;
@synthesize dts = _dts;
@synthesize pos = _pos;
@synthesize serial = _serial;
@synthesize flush = _flush;

- (id) initWithPkt:(AVPacket *) pkt serial:(int) serial isFlush:(BOOL) flush {
    
    self = [super init];
    if (self) {
        _size = pkt->size;
        _pts = pkt->pts;
        _dts = pkt->dts;
        _pos = pkt->pos;
        _serial = serial;
        _flush = flush;
        
        _samples = malloc(_size);
        memset(_samples, 0, _size);
        memcpy(_samples, pkt->data, _size);
    }
    return self;
}


- (void) dealloc {
    if (_samples) {
        free(_samples);
        _samples = NULL;
    }
    [super dealloc];
}

@end
