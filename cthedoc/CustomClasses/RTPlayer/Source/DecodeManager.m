
#import "DecodeManager.h"
#import "AudioDecoder.h"
#import "VideoDecoder.h"
#import "RTReachability.h"

#include "libavformat/avformat.h"
#include "libswscale/swscale.h"
#include "libavutil/opt.h"

#include "libavutil/time.h"

#include "cmdutils.h"


//typedef enum  {
//    RTSPTransportType_UDP,
//    RTSPTransportType_TCP,
//    RTSPTransportType_UDP_Multicast,
//    RTSPTransportType_HTTP
//}RTSPTransportType;

// Decoder decode option keys
NSString *DECODER_OPT_KEY_RTSP_TRANSPORT                      = @"rtsp_transport";
NSString *DECODER_OPT_KEY_AUD_STRM_DEF_IDX                    = @"audio_stream_default_index";
NSString *DECODER_OPT_KEY_AUD_STRM_DEF_STR                    = @"audio_stream_default_str";
NSString *DECODER_OPT_KEY_FORCE_MJPEG                         = @"force_mjpeg";
NSString *DECODER_OPT_KEY_PASS_THROUGH                        = @"pass_through";


// Decoder decode option values
NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP                = @"udp";
NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_TCP                = @"tcp";
NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP_MULTICAST      = @"udp_multicast";
NSString *DECODER_OPT_VALUE_RTSP_TRANSPORT_HTTP               = @"http";



//stream info data keys
NSString *STREAMINFO_KEY_CONNECTION                             = @"stream_info_connection";
NSString *STREAMINFO_KEY_DOWNLOAD                               = @"stream_info_download";
NSString *STREAMINFO_KEY_BITRATE                                = @"stream_info_bitrate";
NSString *STREAMINFO_KEY_AUDIO                                  = @"stream_info_audio";
NSString *STREAMINFO_KEY_VIDEO                                  = @"stream_info_video";

//C variables & functions
static int64_t duration = AV_NOPTS_VALUE;
static int decode_interrupt_cb(void *decoder);
extern const char *av_get_pix_fmt_name(enum AVPixelFormat pix_fmt);

static const OptionDef options[] = {
    { "default", HAS_ARG | OPT_AUDIO | OPT_VIDEO | OPT_EXPERT, { .func_arg = opt_default }, "generic catch all option", "" },
    { NULL, },
};

#ifdef DEBUG
    LogLevel log_level = kLogLevelStateChanges;
#else
    LogLevel log_level = kLogLevelDisable;
#endif



//Audio & Video Codecs parameters
static int idct = FF_IDCT_AUTO;
static enum AVDiscard skip_frame       = AVDISCARD_DEFAULT;
static enum AVDiscard skip_idct        = AVDISCARD_DEFAULT;
static enum AVDiscard skip_loop_filter = AVDISCARD_DEFAULT;
static int error_concealment = 3;
static int workaround_bugs = 1;
static int lowres = 0;

//defines
#define FLAGS(o) ((o)->type == AV_OPT_TYPE_FLAGS) ? AV_DICT_APPEND : 0
#define INVALID_VALUE        -1

//Audio & Video default queue sizes
#define DEFAULT_VIDEO_PICTURE_QUEUE_SIZE            4
#define DEFAULT_MAX_QUEUE_SIZE                      10 * 1024 * 1024
#define DEFAULT_MIN_FRAMES_TO_START_PLAYING         10 /* get packets till this number, higher value increases buffering time */

//Decoder default log frequencies
#define DEFAULT_AV_SYNC_LOG_FREQUENCY               0.01
#define DEFAULT_SLog_PKT_COUNT_SHOW_FREQUENCY      0.01

#define CONFIG_RTSP_DEMUXER                         1
#define CONFIG_MMSH_PROTOCOL                        1

/* external clock speed adjustment constants for realtime sources based on buffer fullness */
#define EXTERNAL_CLOCK_SPEED_MIN                    0.900
#define EXTERNAL_CLOCK_SPEED_MAX                    1.010
#define EXTERNAL_CLOCK_SPEED_STEP                   0.001

#pragma mark -

@interface DecodeManager () {

    AVFormatContext* _avFmtCtx;/* ffmpeg format context */

    NSURL *_streamURL; /* stream url, supported protocols http, mms, rtsp, rtmp */
    NSDictionary *_decodeOptions;

    //dispatch queues
    dispatch_queue_t _readPktQueue;

    //AV Decoders
    VideoDecoder *_vDecoder;
    AudioDecoder *_aDecoder;

    //BOOL _audioIsOK;
    //BOOL _videoIsOK;

    BOOL _readJobIsDone;
    BOOL _lastPaused;
    BOOL _initialBuffer;

    NSUInteger _frameWidth;
    NSUInteger _frameHeight;
    VideoStreamColorFormat _videoStreamColorFormat;

    DecoderState _decoderState;
    BOOL _appIsInBackgroundNow;
    int _readPktErrorCount;

    //Reachability
    RTReachability *_reachability;

    //Stream control values
    BOOL _streamIsPaused;
    int _abortIsRequested;
    int _readPauseCode;

    //Decoder limit parameters
    int _probeSize;
    int _maxAnalyzeDuration;
    BOOL _remoteFileStreaming;
    int _videoPictureQueueSize;
    int _maxQeueueSize;
    int _minFramesToStartPlaying;

    //Decoder logging parameters
    LogLevel _logLevel;
    float _avSyncLogFrequency;
    float _avPacketCountLogFrequency;

    int _avSyncType;
    double external_clock;                   ///< external clock base
    double _externalClockDrift;             ///< external clock base - time (av_gettime) at which we updated external_clock
    int64_t _externalClockTime;             ///< last reference time
    double _externalClockSpeed;             ///< speed of the external clock

    int genpts;
    double _maxFrameDuration;      // maximum duration of a frame - above this, we consider the jump a timestamp discontinuity
    int _realTime;
    int _showStatus;
    int _fast;
    int _infiniteBuffer;

    int _seekRequest;
    int _seekFlags;
    int64_t _seekPosition;
    int64_t _seekRel;

    int _step;
    int _loopPlayback;
    BOOL _autoStopAtEnd;
    float _volumeLevel;

    dispatch_semaphore_t _semaReadThread;
    int _lastVideoStream, _lastAudioStream;
    int _queueAttachmentsReq;

    float _durationInSeconds;

    BOOL _audioIsDisabled;
    BOOL _seekByBytes;
    
    float _videoWidth;
    float _videoHeight;
}

@end


@implementation DecodeManager

#pragma mark - Public variables

@synthesize streamIsPaused = _streamIsPaused;
@synthesize abortIsRequested = _abortIsRequested;
@synthesize readPauseCode = _readPauseCode;

@synthesize frameWidth = _frameWidth;
@synthesize frameHeight = _frameHeight;
@synthesize videoStreamColorFormat = _videoStreamColorFormat;

@synthesize totalBytesDownloaded = _totalBytesDownloaded;
@synthesize streamInfo = _streamInfo;

@synthesize appIsInBackgroundNow = _appIsInBackgroundNow;

@synthesize videoPictureQueueSize = _videoPictureQueueSize;
@synthesize maxQueueSize = _maxQeueueSize;
@synthesize minFramesToStartPlaying = _minFramesToStartPlaying;
@synthesize avSyncLogFrequency = _avSyncLogFrequency;
@synthesize avPacketCountLogFrequency = _avPacketCountLogFrequency;

@synthesize maxFrameDuration = _maxFrameDuration;
@synthesize step = _step;

@synthesize probeSize = _probeSize;
@synthesize maxAnalyzeDuration = _maxAnalyzeDuration;
@synthesize remoteFileStreaming = _remoteFileStreaming;
@synthesize durationInSeconds = _durationInSeconds;
@synthesize audioIsDisabled = _audioIsDisabled;
@synthesize initialPlaybackTime = _initialPlaybackTime;
@synthesize loopPlayback = _loopPlayback;
@synthesize autoStopAtEnd = _autoStopAtEnd;
@synthesize volumeLevel = _volumeLevel;

#pragma mark - Initialization

- (id)init {
    self = [super init];
    if(self) {
        [self initEngine];
        [self initReachability];
        [self createDispatchQueues];
        [self initValues];
        
        _streamInfo = [[NSMutableDictionary alloc] init];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillEnterForeground)
                                                     name:UIApplicationWillEnterForegroundNotification object:nil];

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onReachabilityChanged:) name:kRTReachabilityChangedNotification object:nil];

    }
    return self;
}

- (void)initEngine {
    avcodec_register_all(); //Register all supported codecs, parsers and bitstream filters
    av_register_all(); //Initialize libavformat and register all the muxers, demuxers and protocols.
    avformat_network_init(); // Do global initialization of network components.
}

- (void)initReachability {
    if(_reachability == nil) {
        _reachability = [[RTReachability reachabilityForInternetConnection] retain];
        [_reachability startNotifier];
    }
}

- (void)createDispatchQueues {
    _readPktQueue = dispatch_queue_create("read_dispatch_queue", NULL);
}

- (void)initSemaphore {
    _semaReadThread = dispatch_semaphore_create(0);
}

- (void)initValues {
    
    _avFmtCtx = NULL;
    _decoderState = kDecoderStateNone;
    _appIsInBackgroundNow = NO;
    
    _audioOk = NO;
    _videoOk = NO;

    _abortIsRequested = NO;
    _readPauseCode = 0;
    _initialBuffer = NO;

    _readJobIsDone = YES;

    _frameWidth = 0;
    _frameHeight = 0;

    _readPktErrorCount = 0;
    _totalBytesDownloaded = 0;

    //init default values
    _remoteFileStreaming = NO;
    _videoPictureQueueSize = DEFAULT_VIDEO_PICTURE_QUEUE_SIZE;
    _maxQeueueSize = DEFAULT_MAX_QUEUE_SIZE;
    _minFramesToStartPlaying = DEFAULT_MIN_FRAMES_TO_START_PLAYING;
    _avSyncLogFrequency = DEFAULT_AV_SYNC_LOG_FREQUENCY;
    _avPacketCountLogFrequency = DEFAULT_SLog_PKT_COUNT_SHOW_FREQUENCY;

    [self updateExternalClockPts:NAN];
    [self updateExternalClockSpeed:1.0];
    genpts = 0;
    _seekByBytes = NO;
    _showStatus = 1;
    _fast = 0;
    _infiniteBuffer = -1;
    _seekRequest = 0;
    
    _probeSize = -1;
    _maxAnalyzeDuration = -1;

    _step = 0;
    _loopPlayback = 1;
    _autoStopAtEnd = NO;
    _audioIsDisabled = NO;

    _avSyncType = AV_SYNC_AUDIO_MASTER;
    _lastVideoStream = -1, _lastAudioStream = -1;
    _durationInSeconds = -1.0;
    _initialPlaybackTime = AV_NOPTS_VALUE;
    _videoWidth = 0.0;
    _videoHeight = 0.0;
    
    _videoStreamColorFormat = VideoStreamColorFormatUnknown;
}

#pragma mark - Connection

- (RTError)connectWithStreamURLString:(NSString*)urlString options:(NSDictionary *)options {

    AVDictionary *option = NULL;
    RTError err;
    NSString *urlStringFinal = NULL;
    [self setDecoderState:kDecoderStateConnecting errorCode:kRTErrorNone];
    _realTime = ![self isFileStream:urlString];
    _decodeOptions = [options retain];
    
    BOOL passThrough = [[options objectForKey:DECODER_OPT_KEY_PASS_THROUGH] boolValue];
    
    if (passThrough) {
        err = [self parseOptionsFromURLString:urlString finalURLString:&urlStringFinal];
        if (err != kRTErrorNone)
            return err;
        _streamURL = [NSURL URLWithString:urlStringFinal];
    } else {
        NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        err = (_realTime) ? [self checkProtocolForValidness:url] : kRTErrorNone;
        if (err != kRTErrorNone) {
            return err;
        }
        option = (_realTime) ? [self updateProtocolSettingsWithURL:url] : NULL;
        urlStringFinal = (_realTime) ? [[_streamURL description] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] : urlString;
    }
    
    err = [self openStreamURLString:urlStringFinal option:option];
    if (err != kRTErrorNone)
        return err;

    err = [self checkStreamsForURLString:urlStringFinal];
    if (err != kRTErrorNone)
        return err;

    _durationInSeconds = _avFmtCtx->duration / 1000000LL;
    
    if (_durationInSeconds <= 0.0)
        [self setDecoderState:kDecoderStateGotStreamDuration errorCode:kRTErrorStreamDurationNotFound];
    else
        [self setDecoderState:kDecoderStateGotStreamDuration errorCode:kRTErrorNone];

    [self setDecoderState:kDecoderStateConnected errorCode:kRTErrorNone];
    
    err = [self openAVStreams];
    if (err != kRTErrorNone)
        return err;

    [self performSelector:@selector(onReachabilityChanged:) withObject:nil];

    return kRTErrorNone;
}

- (RTError)parseOptionsFromURLString:(NSString *)urlString
                      finalURLString:(NSString **)finalURLString {
    
    NSArray *params = [urlString componentsSeparatedByString:@" "];
    if (!params || !params.count ||
        ![params objectAtIndex:0] || ![[params objectAtIndex:0] length]) {
        return kRTErrorStreamURLParseError;
    }
    *finalURLString = [params objectAtIndex:0];
    
    const char *opt;
    int ret = 0;
    int handleOptions = 1;
    NSUInteger count = params.count;
    
    for(int optindex = 1; optindex < count;) {
        opt = [[params objectAtIndex:optindex++] UTF8String];
        
        if (handleOptions && opt[0] == '-' && opt[1] != '\0') {
            if (opt[1] == '-' && opt[2] == '\0') {
                handleOptions = 0;
                continue;
            }
            opt++;
            
            if ((ret = parse_option(NULL, opt, [[params objectAtIndex:optindex] UTF8String], options)) < 0)
                return kRTErrorStreamURLParseError;
            optindex += ret;
        }
    }
    return kRTErrorNone;
}

- (RTError)checkProtocolForValidness:(NSURL *)url {
    if(![[url scheme] isEqualToString:@"mms"]  &&
       ![[url scheme] isEqualToString:@"mmsh"] &&
       ![[url scheme] isEqualToString:@"mmst"] &&
       ![[url scheme] isEqualToString:@"http"] &&
       ![[url scheme] isEqualToString:@"rtsp"] &&
       ![[url scheme] isEqualToString:@"rtp"] &&
       ![[url scheme] isEqualToString:@"rtmp"] &&
       ![[url scheme] isEqualToString:@"rtmps"]) {
        [self setDecoderState:kDecoderStateConnectionFailed errorCode:kRTErrorUnsupportedProtocol];
        return kRTErrorUnsupportedProtocol;
    }
    return kRTErrorNone;
}

- (AVDictionary*)updateProtocolSettingsWithURL:(NSURL*) url {
    NSURL *newURL;
    if([[url scheme] isEqualToString:@"mms"]) {
        NSString *urlString = [url description];
        urlString = [urlString stringByReplacingOccurrencesOfString:@"mms://" withString:@"mmst://"];
        newURL = [NSURL URLWithString:urlString];
    } else {
        newURL = url;
    }
    _streamURL = [newURL retain];

    AVDictionary *option_dict = NULL;
    if ([[_streamURL scheme] isEqualToString:@"rtsp"]) {
        const struct AVOption *of = NULL;
        const AVClass *fc = avformat_get_class();

        //TCP is the default transport layer for RTSP protocol unless decoder options are given.
        const char *key = [DECODER_OPT_KEY_RTSP_TRANSPORT UTF8String];
        const char *val = [DECODER_OPT_VALUE_RTSP_TRANSPORT_TCP UTF8String];

        if (_decodeOptions && [_decodeOptions count]) {
            NSString *valStr = [_decodeOptions objectForKey:DECODER_OPT_KEY_RTSP_TRANSPORT];
            if (valStr &&
                ([valStr isEqualToString:DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP] ||
                 [valStr isEqualToString:DECODER_OPT_VALUE_RTSP_TRANSPORT_UDP_MULTICAST] ||
                 [valStr isEqualToString:DECODER_OPT_VALUE_RTSP_TRANSPORT_HTTP])) {
                val = (char *)[valStr UTF8String];
            }
        }
        
        if ((of = av_opt_find(&fc, key, NULL, 0,
                              AV_OPT_SEARCH_CHILDREN | AV_OPT_SEARCH_FAKE_OBJ))){
            av_dict_set(&option_dict, key, val, FLAGS(of));
        }
    } else if ([[_streamURL scheme] isEqualToString:@"rtmp"] || [[_streamURL scheme] isEqualToString:@"rtmps"]) {

        NSArray *array = [[[_streamURL description] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] componentsSeparatedByString:@"&"];
        if (array.count == 2) {
            [_streamURL release];
            _streamURL = [[NSURL URLWithString:[array objectAtIndex:0]] retain];

            const struct AVOption *of = NULL;
            const AVClass *fc = avformat_get_class();
            const char *key = "rtmp_swfurl";
            const char *val = [[[array objectAtIndex:1] substringFromIndex:4] UTF8String];

            if ((of = av_opt_find(&fc, key, NULL, 0,
                                  AV_OPT_SEARCH_CHILDREN | AV_OPT_SEARCH_FAKE_OBJ))){
                av_dict_set(&option_dict, key, val, FLAGS(of));
            }
        }
    }
    return option_dict;
}

- (RTError)openStreamURLString:(NSString *)urlString option:(AVDictionary *)option {

    if (_abortIsRequested) {
        return kRTErrorStreamsNotAvailable;
    }

    _avFmtCtx = avformat_alloc_context();
    _avFmtCtx->interrupt_callback.callback = decode_interrupt_cb;
    _avFmtCtx->interrupt_callback.opaque = self;
    SLog(kLogLevelDecoder, @"_avFmtCtx is allocated now");
    
    BOOL passThrough = [[_decodeOptions objectForKey:DECODER_OPT_KEY_PASS_THROUGH] boolValue];
    
    const char *input = [urlString UTF8String];
    
    NSLog(@"URL before open input = %@", urlString);
    
    if (passThrough) {
        if(avformat_open_input(&_avFmtCtx, input, NULL, &format_opts) < 0){
            [self setDecoderState:kDecoderStateConnectionFailed errorCode:kRTErrorOpenStream];
            return kRTErrorOpenStream;
        }
    } else {
        if (_probeSize != -1) {
            _avFmtCtx->probesize = _probeSize;
        }
        
        if (_maxAnalyzeDuration != -1) {
            _avFmtCtx->max_analyze_duration = _maxAnalyzeDuration;
        }
        
        if (_decodeOptions && [_decodeOptions objectForKey:DECODER_OPT_KEY_FORCE_MJPEG]) {
            _avFmtCtx->iformat = av_find_input_format("mjpeg");
            _avFmtCtx->probesize = (_probeSize != -1) ? _probeSize : 32;
            _avFmtCtx->max_analyze_duration = 0;
        }
        
        if(avformat_open_input(&_avFmtCtx, input, NULL, &option) < 0){
            [self setDecoderState:kDecoderStateConnectionFailed errorCode:kRTErrorOpenStream];
            return kRTErrorOpenStream;
        }
    }
    
    //Generate missing pts even if it requires parsing future frames
    if (genpts)
        _avFmtCtx->flags |= AVFMT_FLAG_GENPTS;
    
    _avFmtCtx->flags |= AVFMT_FLAG_NONBLOCK;

    _maxFrameDuration = (_avFmtCtx->iformat->flags & AVFMT_TS_DISCONT) ? 10.0 : 3600.0;

    /* if seeking requested, we execute it */
    if (_initialPlaybackTime != AV_NOPTS_VALUE) {
        int64_t timestamp;

        timestamp = _initialPlaybackTime;
        /* add the stream start time */
        if (_avFmtCtx->start_time != AV_NOPTS_VALUE)
            timestamp += _avFmtCtx->start_time;
        int ret = avformat_seek_file(_avFmtCtx, -1, INT64_MIN, timestamp, INT64_MAX, 0);
        if (ret < 0) {
            fprintf(stderr, "could not seek to position %0.3f\n", (double)timestamp / AV_TIME_BASE);
        }
    }
    return kRTErrorNone;
}

- (RTError)checkStreamsForURLString:(NSString *)urlString {

    if (_abortIsRequested) {
        return kRTErrorStreamsNotAvailable;
    }

    AVDictionary **opts;
    opts = setup_find_stream_info_opts(_avFmtCtx, codec_opts);
    int originalNumberStreams = _avFmtCtx->nb_streams;
    
    //Check the streams
    if(avformat_find_stream_info(_avFmtCtx, opts) < 0) {
        [self setDecoderState:kDecoderStateConnectionFailed errorCode:kRTErrorStreamInfoNotFound];
        return kRTErrorStreamInfoNotFound;
    }

    for (int i = 0; i < originalNumberStreams; i++){
        if (&opts[i]) {
            av_dict_free(&opts[i]);
        }
    }

    av_freep(&opts);

    if (_showStatus) {
        SLog(kLogLevelDecoder, @"Stream info exists:");
        av_dump_format(_avFmtCtx, 0, [urlString UTF8String], 0);
    }

    if (_avFmtCtx->pb)
        _avFmtCtx->pb->eof_reached = 0; //fix from ffplay

    return kRTErrorNone;
}

#pragma mark - Streams management

- (RTError)openAVStreams {

    if (_abortIsRequested) {
        return kRTErrorStreamsNotAvailable;
    }

    for (int i = 0; i < _avFmtCtx->nb_streams; i++)
        _avFmtCtx->streams[i]->discard = AVDISCARD_ALL;

    if (_avFmtCtx->bit_rate)
        [_streamInfo setValue: [NSNumber numberWithInt:_avFmtCtx->bit_rate]
                       forKey:STREAMINFO_KEY_BITRATE];

    NSInteger aStreamId = INVALID_VALUE;
    NSInteger vStreamId = INVALID_VALUE;

    NSNumber *aStreamOptIndex = [_decodeOptions objectForKey:DECODER_OPT_KEY_AUD_STRM_DEF_IDX];
    BOOL hasDefaultAudStreamIdx = NO;
    if (aStreamOptIndex) {
        hasDefaultAudStreamIdx = YES;
    }

    NSString *aStreamOptStr = [_decodeOptions objectForKey:DECODER_OPT_KEY_AUD_STRM_DEF_STR];
    BOOL hasDefaultAudioStreamStr = NO;
    if (aStreamOptStr) {
        hasDefaultAudioStreamStr = YES;
    }

    BOOL audioStreamIsSelected = NO;

    for (int sIdx = 0; sIdx < _avFmtCtx->nb_streams; sIdx++) {

        AVStream *stream = _avFmtCtx->streams[sIdx];
        AVCodecContext *codec = stream->codec;
        
        if (!_audioIsDisabled && (codec->codec_type == AVMEDIA_TYPE_AUDIO && codec->channels > 0) && !audioStreamIsSelected) {

            AVDictionaryEntry *langEntry = av_dict_get(stream->metadata, "language", NULL, 0);
            NSString *lang = @"default_language";
            if (langEntry && langEntry->value) {
                lang = [NSString stringWithFormat:@"%s", langEntry->value];
            }
        
            if (hasDefaultAudioStreamStr && !hasDefaultAudStreamIdx) {
                if ([lang isEqualToString:[_decodeOptions objectForKey:DECODER_OPT_KEY_AUD_STRM_DEF_STR]]) {
                    audioStreamIsSelected = YES;
                }
            }

            if (hasDefaultAudStreamIdx) {
                if (stream->id == [[_decodeOptions objectForKey:DECODER_OPT_KEY_AUD_STRM_DEF_IDX] intValue]) {
                    audioStreamIsSelected = YES;
                }
            }

            //If no default is given set selected audio stream index as the first audio stream
            if (!(hasDefaultAudioStreamStr || hasDefaultAudStreamIdx)) {
                audioStreamIsSelected = YES;
            }
            aStreamId = sIdx;
            [self updateStreamInfoWithSelectedStreamIndex: (int)aStreamId type:AVMEDIA_TYPE_AUDIO];
        } else if (codec->codec_type == AVMEDIA_TYPE_VIDEO){
            vStreamId = sIdx;
            _videoWidth = _avFmtCtx->streams[vStreamId]->codec->coded_width;
            _videoHeight = _avFmtCtx->streams[vStreamId]->codec->coded_height;
            
            //NSLog(@"video width = %.2f, height = %.2f", _videoWidth, _videoHeight);
            
            [self updateStreamInfoWithSelectedStreamIndex: (int)vStreamId type:AVMEDIA_TYPE_VIDEO];
        }
    }

    /* Check audio codec and initialize audio decoder */
    RTError errAudio = kRTErrorNone;
    if (aStreamId != INVALID_VALUE) {
        errAudio = [self openAudioStreamWithId:aStreamId];
        if (errAudio == kRTErrorNone){
            _avFmtCtx->streams[aStreamId]->discard = AVDISCARD_DEFAULT;
            _audioOk = YES;
        }
    } else {
        errAudio = kRTErrorAudioStreamNotFound;
    }
    [self setDecoderState:kDecoderStateGotAudioStreamInfo errorCode:errAudio];

    /* Check video codec and initialize video decoder */
    RTError errVideo = kRTErrorNone;
    if (vStreamId != INVALID_VALUE) {
        errVideo = [self openVideoStreamWithId:vStreamId audioIsOK:_audioOk];
        if(errVideo == kRTErrorNone) {
            _avFmtCtx->streams[vStreamId]->discard = AVDISCARD_DEFAULT;
            _videoOk = YES;
        }
    } else {
        errVideo = kRTErrorVideoStreamNotFound;
    }
    [self setDecoderState:kDecoderStateGotVideoStreamInfo errorCode:errVideo];

    if ((errAudio != kRTErrorNone) && (errVideo != kRTErrorNone))
        return kRTErrorStreamsNotAvailable;

    return kRTErrorNone;
}

- (RTError)openAudioStreamWithId:(NSInteger)sId {

    if (_audioOk) {
        return kRTErrorAudioStreamAlreadyOpened;
    }

    AVDictionary *opts;
    AVDictionaryEntry *t = NULL;
    AVCodecContext *audCdcCtx = _avFmtCtx->streams[sId]->codec;
    AVCodec *audCdc = avcodec_find_decoder(audCdcCtx->codec_id);

    if(!audCdc) {
        return kRTErrorAudioCodecNotFound;
    }

    audCdcCtx->workaround_bugs   = workaround_bugs;
    audCdcCtx->lowres            = lowres;
    if(audCdcCtx->lowres > audCdc->max_lowres) {
        audCdcCtx->lowres = audCdc->max_lowres;
    }

    audCdcCtx->idct_algo         = idct;
    audCdcCtx->skip_frame        = skip_frame;
    audCdcCtx->skip_idct         = skip_idct;
    audCdcCtx->skip_loop_filter  = skip_loop_filter;
    audCdcCtx->error_concealment = error_concealment;

    if(audCdcCtx->lowres)
        audCdcCtx->flags |= CODEC_FLAG_EMU_EDGE;
    if (_fast)
        audCdcCtx->flags2 |= CODEC_FLAG2_FAST;

    if(audCdc->capabilities & CODEC_CAP_DR1)
        audCdcCtx->flags |= CODEC_FLAG_EMU_EDGE;

    opts = filter_codec_opts(codec_opts, audCdcCtx->codec_id, _avFmtCtx, _avFmtCtx->streams[sId], audCdc);
    if (!av_dict_get(opts, "threads", NULL, 0))
        av_dict_set(&opts, "threads", "auto", 0);

    if(avcodec_open2(audCdcCtx, audCdc, &opts)){
        return kRTErrorAudioCodecNotOpened;
    }

    if ((t = av_dict_get(opts, "", NULL, AV_DICT_IGNORE_SUFFIX))) {
        av_log(NULL, AV_LOG_ERROR, "Option %s not found.\n", t->key);
        return AVERROR_OPTION_NOT_FOUND;
    }

    _aDecoder = [[AudioDecoder alloc] initWithCodecContext:audCdcCtx
                                                      stream:_avFmtCtx->streams[sId] streamId:sId manager:self];
    if (!_aDecoder) {
        return kRTErrorAudioAllocateMemory;
    }
    SLog(kLogLevelDecoder, @"audio codec smr: %.d fmt: %d chn: %d",
         audCdcCtx->sample_rate, audCdcCtx->sample_fmt, audCdcCtx->channels);

    _lastAudioStream = (int)sId;
    
    return kRTErrorNone;
}

- (RTError)openVideoStreamWithId:(NSInteger)sId audioIsOK:(BOOL)audioIsOK {

    AVDictionary *opts;
    AVDictionaryEntry *t = NULL;

    AVCodecContext *vidCdcCtx = _avFmtCtx->streams[sId]->codec;
    AVCodec *vidCdc = avcodec_find_decoder(vidCdcCtx->codec_id);

    if(!vidCdc) {
        SLog(kLogLevelDecoder, @"Video codec is not found");
        return kRTErrorVideoCodecNotFound;
    }

    vidCdcCtx->workaround_bugs   = workaround_bugs;
    vidCdcCtx->lowres            = lowres;
    if(vidCdcCtx->lowres > vidCdc->max_lowres){
        vidCdcCtx->lowres= vidCdc->max_lowres;
    }

    vidCdcCtx->idct_algo         = idct;
    vidCdcCtx->skip_frame        = skip_frame;
    vidCdcCtx->skip_idct         = skip_idct;
    vidCdcCtx->skip_loop_filter  = skip_loop_filter;
    vidCdcCtx->error_concealment = error_concealment;

    if(vidCdcCtx->lowres)
        vidCdcCtx->flags |= CODEC_FLAG_EMU_EDGE;
    if (_fast)
        vidCdcCtx->flags2 |= CODEC_FLAG2_FAST;

    if(vidCdc->capabilities & CODEC_CAP_DR1)
        vidCdcCtx->flags |= CODEC_FLAG_EMU_EDGE;

    opts = filter_codec_opts(codec_opts, vidCdcCtx->codec_id, _avFmtCtx, _avFmtCtx->streams[sId], vidCdc);
    if (!av_dict_get(opts, "threads", NULL, 0))
        av_dict_set(&opts, "threads", "auto", 0);

    if(avcodec_open2(vidCdcCtx, vidCdc, &opts)){
        SLog(kLogLevelDecoder, @" Video codec is not opened");
        return kRTErrorVideoCodecNotOpened;
    }

    if ((t = av_dict_get(opts, "", NULL, AV_DICT_IGNORE_SUFFIX))) {
        av_log(NULL, AV_LOG_ERROR, "Option %s not found.\n", t->key);
        return AVERROR_OPTION_NOT_FOUND;
    }

    _frameWidth = vidCdcCtx->width;
    _frameHeight = vidCdcCtx->height;
    const char *formatCStr = (const char *)av_get_pix_fmt_name(vidCdcCtx->pix_fmt);
    NSString *formatStr = (formatCStr) ? [NSString stringWithUTF8String:formatCStr] : @"";
    if (formatStr && [formatStr length]) {
        if ([formatStr rangeOfString:@"yuv"].location == NSNotFound) {
            _videoStreamColorFormat = VideoStreamColorFormatRGB;
        } else {
            _videoStreamColorFormat = VideoStreamColorFormatYUV;
        }
    }

    _vDecoder = [[VideoDecoder alloc] initWithFormatContext:_avFmtCtx codecContext:vidCdcCtx stream:_avFmtCtx->streams[sId] streamId:sId manager:self audioDecoder:_aDecoder];
    
    if (!_vDecoder) {
        SLog(kLogLevelDecoder, @"Video decoder can not be allocated");
        return kRTErrorVideoAllocateMemory;
    }

    _lastVideoStream = (int)sId;
    
    return kRTErrorNone;
}

- (void)closeAudioStream {
    if (_audioOk) {
        _audioOk = NO;
        [_aDecoder shutdown];
        [_aDecoder release];
        _aDecoder = NULL;
    }
}

- (void)closeVideoStream {
    if (_videoOk) {
        _videoOk = NO;
        [_vDecoder shutdown];
        [_vDecoder release];
        _vDecoder = NULL;
    }
}

#pragma mark - Read & control packets

- (void)start {
    if (!_abortIsRequested) {
        SLog(kLogLevelDecoder, @"DecodeManager is now starting to read packets");
        [self initSemaphore];
        [self setDecoderState:kDecoderStateInitialLoading errorCode:kRTErrorNone];
        [_vDecoder performSelector: NSSelectorFromString(@"decodeVideo")];
        [self performSelector:@selector(readPackets)];
    }
}

- (void) readPackets {

    dispatch_async(_readPktQueue, ^(void) {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

        _readJobIsDone = NO;
        AVPacket packet;
        int err = 0;
        int pktInPlayRange = 0;
        int eof = 0;
        
        int audPktCount = 0;
        int vidPktCount = 0;
        int aQueueSize = 0;
        int vQueueSize = 0;

        while (_avFmtCtx->packet_buffer != _avFmtCtx->packet_buffer_end) {
            
            err = av_read_frame(_avFmtCtx, &packet);
            av_free_packet(&packet);
        }
        
        if (_infiniteBuffer < 0 && _realTime)
            _infiniteBuffer = 1;

        for (;;) {
            if (_abortIsRequested)
                break;
            if (_streamIsPaused != _lastPaused) {
                _lastPaused = _streamIsPaused;
                if (_streamIsPaused)
                    _readPauseCode = av_read_pause(_avFmtCtx);
                else
                    av_read_play(_avFmtCtx);
            }

#if CONFIG_RTSP_DEMUXER || CONFIG_MMSH_PROTOCOL
            if (_realTime && _streamIsPaused &&
                (!strcmp(_avFmtCtx->iformat->name, "rtsp") ||
                 (_avFmtCtx->pb && !strncmp([[[_streamURL description] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding] UTF8String], "mmsh:", 5)))) {
                    /* wait 10 ms to avoid trying to get another packet for above protocols */
                    [NSThread sleepForTimeInterval:0.01];
                    continue;
                }
#endif

            if (_seekRequest) {
                int64_t seek_target = _seekPosition;
                int64_t seek_min    = _seekRel > 0 ? seek_target - _seekRel + 2: INT64_MIN;
                int64_t seek_max    = _seekRel < 0 ? seek_target - _seekRel - 2: INT64_MAX;
                // FIXME the +-2 is due to rounding being not done in the correct direction in generation
                //      of the _seekPosition/_seekRel variables

                int ret = avformat_seek_file(_avFmtCtx, -1, seek_min, seek_target, seek_max, _seekFlags);
                if (ret < 0) {
                    fprintf(stderr, "%s: error while seeking\n", _avFmtCtx->filename);
                } else {
                    if (_audioOk) {
                        pthread_mutex_lock([_aDecoder mutexPkt]);
                        [_aDecoder clearPktQueue];
                        [_aDecoder addFlushPkt];
                        pthread_mutex_unlock([_aDecoder mutexPkt]);
                    }
                    if (_videoOk) {
                        pthread_mutex_lock([_vDecoder mutexPkt]);
                        [_vDecoder clearPktQueue];
                        [_vDecoder addFlushPkt];
                        pthread_mutex_unlock([_vDecoder mutexPkt]);
                    }
                    if (_seekFlags & AVSEEK_FLAG_BYTE) {
                        [self updateExternalClockPts:NAN];
                    } else {
                        [self updateExternalClockPts:(seek_target / (double)AV_TIME_BASE)];
                    }
                }

                _seekRequest = 0;
                eof = 0;

                if (_streamIsPaused)
                    [self stepToNextFrame];

                if (_queueAttachmentsReq) {
                    avformat_queue_attached_pictures(_avFmtCtx);
                    _queueAttachmentsReq = 0;
                }
            }

            int static pktCountIter = 0;
            int modPktCount = 1/_avPacketCountLogFrequency;
            signed int totalSize = 0.0;
            
            audPktCount = 0;
            vidPktCount = 0;
            aQueueSize = 0;
            vQueueSize = 0;
            
            if (_audioOk) {
                audPktCount = (int)[[_aDecoder pktQueue] count];
                aQueueSize = (int)[_aDecoder pktQueueSize];
            }
            if (_videoOk) {
                vidPktCount = (int)[[_vDecoder pktQueue] count];
                vQueueSize = (int)[_vDecoder pktQueueSize];
            }
            totalSize = aQueueSize + vQueueSize;

            if((log_level & kLogLevelDecoderExtra) && (pktCountIter%modPktCount == 0)){//
                pktCountIter = 0;

                printf("[vq=%d (%0.2f KB)  -   aq=%d (%0.2f KB)] - totalsize= %0.2f MB\n",
                       vidPktCount, (float)(vQueueSize)/1024.0,
                       audPktCount, (float)(aQueueSize)/1024.0,
                       (float)totalSize/1048576.0/*1024*1024*/);
            }
            pktCountIter++;
            

            if (_infiniteBuffer < 1 && ((totalSize > _maxQeueueSize) ||
                                        (((!_audioOk) || [[_aDecoder pktQueue] count] > _minFramesToStartPlaying) && ((!_videoOk) || [[_vDecoder pktQueue] count] > _minFramesToStartPlaying)))) {
                    [NSThread sleepForTimeInterval:0.01];
                    continue;
            } else if (_infiniteBuffer >= 1 && (totalSize > _maxQeueueSize/2)) {
                [NSThread sleepForTimeInterval:0.01];
                continue;
            }

            if (eof) {
                if (_videoOk) {
                    av_init_packet(&packet);
                    packet.data = NULL;
                    packet.size = 0;
                    packet.stream_index = (int)[_vDecoder streamId];
                    pthread_mutex_lock([_vDecoder mutexPkt]);
                    [_vDecoder addPacket:&packet];
                    pthread_cond_signal([_vDecoder condPkt]);
                    pthread_mutex_unlock([_vDecoder mutexPkt]);
                }

                if (_audioOk) {
                    AVStream *stream = _avFmtCtx->streams[[_aDecoder streamId]];
                    if (stream->codec->codec->capabilities & CODEC_CAP_DELAY) {
                        av_init_packet(&packet);
                        packet.data = NULL;
                        packet.size = 0;
                        packet.stream_index = (int)[_aDecoder streamId];
                        [_aDecoder addPacket:&packet];

                        pthread_mutex_lock([_aDecoder mutexPkt]);
                        [_aDecoder addPacket:&packet];
                        pthread_cond_signal([_aDecoder condPkt]);
                        pthread_mutex_unlock([_aDecoder mutexPkt]);
                    }
                }

                [NSThread sleepForTimeInterval:0.01];

                if (totalSize == 0) {
                    if (_loopPlayback != 1 && (!_loopPlayback || --_loopPlayback)) {
                        [self streamSeek:(_initialPlaybackTime != AV_NOPTS_VALUE ? _initialPlaybackTime : 0) rel:0 byBytes:0];
                    } else if (_autoStopAtEnd) {
                        err = AVERROR_EOF;
                        SLog(kLogLevelDecoder, @"End of file, nothing to be read  !!!");
                        [self setDecoderState:kDecoderStateStoppedWithError errorCode:kRTErrorStreamEOFError];
                        break;
                    }
                }
                
                eof = 0;
                continue;
            }

            err = av_read_frame(_avFmtCtx, &packet);
            
            BOOL playingTimeExpired = NO;

            if (err < 0 || playingTimeExpired) {
                if (packet.data) {
                    av_free_packet(&packet);
                }
                
                if (err < 0 || playingTimeExpired) {
                    if (err == AVERROR_EOF || url_feof(_avFmtCtx->pb)) {
                        eof = 1;
                        if (_videoOk) [_vDecoder setEOF:eof];
                        if (_audioOk) [_aDecoder setEOF:eof];
                    }
                    if ((_avFmtCtx->pb && _avFmtCtx->pb->error) || playingTimeExpired) {
                        SLog(kLogLevelDecoder, @"Read ERROR !!!");
                        [self setDecoderState:kDecoderStateStoppedWithError errorCode:kRTErrorStreamReadError];
                        break;
                    }

                    [NSThread sleepForTimeInterval:0.01];
                    continue;
                }
            }
            if (_videoOk) [_vDecoder setEOF:eof];
            if (_audioOk) [_aDecoder setEOF:eof];

            int64_t val1 = (packet.pts - _avFmtCtx->streams[packet.stream_index]->start_time) * av_q2d(_avFmtCtx->streams[packet.stream_index]->time_base);
            double val2 = (double)(_initialPlaybackTime != AV_NOPTS_VALUE ? _initialPlaybackTime : 0) / 1000000;
            pktInPlayRange = duration == AV_NOPTS_VALUE || val1 - val2 <= ((double)duration / 1000000);

            if (_audioOk && (packet.stream_index == [_aDecoder streamId]) && pktInPlayRange) {
                pthread_mutex_lock([_aDecoder mutexPkt]);
                [_aDecoder addPacket:(AVPacket *)&packet];

                if (!_initialBuffer) {
                    if ([[_aDecoder pktQueue] count] > (_minFramesToStartPlaying)) {
                        _initialBuffer = YES;
                        [self setDecoderState:kDecoderStateReadyToPlay errorCode:kRTErrorNone];
                        [_aDecoder startAudioSystem];
                        [_vDecoder performSelectorOnMainThread:@selector(schedulePicture) withObject:nil waitUntilDone:NO];
                        [self setDecoderState:kDecoderStatePlaying errorCode:kRTErrorNone];
                    }
                } else {
                    if ([_aDecoder isWaitingForPackets]) {
                        if ([[_aDecoder pktQueue] count] > (_minFramesToStartPlaying)) {
                            [self setDecoderState:kDecoderStatePlaying errorCode:kRTErrorNone];
                            pthread_cond_signal([_aDecoder condPkt]);
                        } else {
                            [self setDecoderState:kDecoderStateBuffering errorCode:kRTErrorNone];
                        }
                    }
                }
                pthread_mutex_unlock([_aDecoder mutexPkt]);

            } else if ((_videoOk) && (packet.stream_index == [_vDecoder streamId]) && pktInPlayRange) {

                pthread_mutex_lock([_vDecoder mutexPkt]);
                [_vDecoder addPacket:(AVPacket *)&packet];

                pthread_cond_signal([_vDecoder condPkt]);
                pthread_mutex_unlock([_vDecoder mutexPkt]);

                if (!_audioOk && !_initialBuffer) {
                    if ([[_vDecoder pktQueue] count] > (_minFramesToStartPlaying-1)) {
                        _initialBuffer = YES;
                        [self setDecoderState:kDecoderStateReadyToPlay errorCode:kRTErrorNone];
                        [_vDecoder performSelectorOnMainThread:@selector(schedulePicture) withObject:nil waitUntilDone:NO];
                        [self setDecoderState:kDecoderStatePlaying errorCode:kRTErrorNone];
                    }
                }
            }
            if (packet.data) {
                _totalBytesDownloaded += packet.size;
                av_free_packet(&packet);
            }
        }

        /* wait until the end */
        while (!_abortIsRequested) {
            [NSThread sleepForTimeInterval:0.1];
        }

        [self shutDown];

        SLog(kLogLevelDecoder, @"readPackets is ENDED!");
        _readJobIsDone = YES;

        dispatch_semaphore_signal(_semaReadThread);
        [pool release];
    });

}

#pragma mark - Decoder logging

- (void)setLogLevel:(LogLevel)logLevel {
    _logLevel = logLevel;
    log_level = _logLevel;
}

#pragma mark - AV syncing

- (int)masterSyncType {
    if (_avSyncType == AV_SYNC_VIDEO_MASTER) {
        if (_videoOk)
            return AV_SYNC_VIDEO_MASTER;
        else
            return AV_SYNC_AUDIO_MASTER;
    } else if (_avSyncType == AV_SYNC_AUDIO_MASTER) {
        if (_audioOk)
            return AV_SYNC_AUDIO_MASTER;
        else
            return AV_SYNC_EXTERNAL_CLOCK;
    }
    return AV_SYNC_EXTERNAL_CLOCK;
}

/* get the current master clock value */
- (double)masterClock
{
    double val = 0.0;

    switch ([self masterSyncType]) {
        case AV_SYNC_VIDEO_MASTER:
            val = [_vDecoder videoClock];
            break;
        case AV_SYNC_AUDIO_MASTER:
            val = [_aDecoder audioClock];
            break;
        default:
            val = [self externalClock];
            break;
    }
    return val;
}

- (double)clockDifference {
    double vClock = 0.0 , aClock = 0.0;

    if (_vDecoder) {
        vClock = [_vDecoder videoClock];
    }
    if (_aDecoder) {
        aClock = [_aDecoder audioClock];
    }
    return (aClock - vClock);
}

- (double)externalClock
{
    if (_streamIsPaused) {
        return external_clock;
    } else {
        double time = av_gettime() / 1000000.0;
        return _externalClockDrift + time - (time - _externalClockTime / 1000000.0) * (1.0 - _externalClockSpeed);
    }
}

- (void)updateExternalClockPts:(double) pts
{
    _externalClockTime = av_gettime();
    external_clock = pts;
    _externalClockDrift = pts - _externalClockTime / 1000000.0;
}

- (void)checkExternalClockSync:(double) pts {
    double ext_clock = [self externalClock];
    if (isnan(ext_clock) || fabs(ext_clock - pts) > AV_NOSYNC_THRESHOLD) {
        [self updateExternalClockPts:pts];
    }
}

- (void)updateExternalClockSpeed:(double) speed {
    [self updateExternalClockPts:[self externalClock]];
    _externalClockSpeed = speed;
}

- (void)checkExternalClockSpeed {
    if ((_videoOk && ([[_vDecoder pktQueue] count] <= _minFramesToStartPlaying / 2)) ||
        (_audioOk &&  ([[_aDecoder pktQueue] count] <= _minFramesToStartPlaying / 2))) {
        [self updateExternalClockSpeed:FFMAX(EXTERNAL_CLOCK_SPEED_MIN, _externalClockSpeed - EXTERNAL_CLOCK_SPEED_STEP)];
    } else if ((!_videoOk || [[_vDecoder pktQueue] count] > _minFramesToStartPlaying * 2) &&
               (!_audioOk || [[_aDecoder pktQueue] count] > _minFramesToStartPlaying * 2)) {
        [self updateExternalClockSpeed:FFMIN(EXTERNAL_CLOCK_SPEED_MAX, _externalClockSpeed + EXTERNAL_CLOCK_SPEED_STEP)];
    } else {
        double speed = _externalClockSpeed;
        if (speed != 1.0)
            [self updateExternalClockSpeed:(speed + EXTERNAL_CLOCK_SPEED_STEP * (1.0 - speed) / fabs(1.0 - speed))];
    }
}

#pragma mark - Public Actions

- (void)togglePause
{
    [self streamTogglePause];
    _step = 0;
}

- (void)stepToNextFrame
{
    /* if the stream is paused unpause it, then step */
    if (_streamIsPaused)
        [self streamTogglePause];
    _step = 1;
}

- (void)abort {

    if (_audioOk && _aDecoder) {
        [_aDecoder setAbortRequest:1];
    }
    if (_videoOk && _vDecoder) {
        [_vDecoder setAbortRequest:1];
    }
    _abortIsRequested = YES;
}

- (void)stop {
    if (_semaReadThread) {
        dispatch_semaphore_wait(_semaReadThread, DISPATCH_TIME_FOREVER);
        dispatch_release(_semaReadThread);
        _semaReadThread = NULL;
    } else {
        [self shutDown];
    }
    [self setDecoderState:kDecoderStateStoppedByUser errorCode:kRTErrorNone];

    SLog(kLogLevelDecoder, @"-==@  DecodeManager is stopped safely  @==-  !!!");
}

- (void)doSeek:(double)value {

    double pos = 0.0;
    if (_seekByBytes) {
        if (_videoOk && [_vDecoder currentPos] >= 0) {
            pos = [_vDecoder currentPos];
        } else if (_audioOk && [_aDecoder currentPos] >= 0) {
            pos = [_aDecoder currentPos];
        } else
            pos = avio_tell(_avFmtCtx->pb);
        if (_avFmtCtx->bit_rate)
            value *= _avFmtCtx->bit_rate / 8.0;
        else
            value *= 180000.0;
        pos += value;
        [self streamSeek:pos rel:value byBytes:1];
    } else {
        pos = [self masterClock];
        if (isnan(pos))
            pos = (double)_seekPosition / AV_TIME_BASE;
        pos = value;
        if (_avFmtCtx->start_time != AV_NOPTS_VALUE && pos < _avFmtCtx->start_time / (double)AV_TIME_BASE)
            pos = _avFmtCtx->start_time / (double)AV_TIME_BASE;
        [self streamSeek:(int64_t)(pos * AV_TIME_BASE) rel:(int64_t)(value * AV_TIME_BASE) byBytes:0];
    }
}

- (void)seekInDedoderBufferByValue:(float) value {
    if (value > 1.0) value = 1.0;
    if (_seekByBytes || _avFmtCtx->duration <= 0) {
        uint64_t size =  avio_size(_avFmtCtx->pb);
        [self streamSeek:(size * value) rel:0 byBytes:1];
    }
}

- (void)cycleAudioStream {
    [self togglePause];
    [self cycleStreamWithType:AVMEDIA_TYPE_AUDIO index:-1];
    [self togglePause];
}

- (void)cycleAudioStreamWithStreamIndex:(int) index {
    [self togglePause];
    [self cycleStreamWithType:AVMEDIA_TYPE_AUDIO index:index];
    [self togglePause];
}

- (NSString *)codecInfoWithStreamIndex:(int) index {

    if (_avFmtCtx && (index < _avFmtCtx->nb_streams)) {
        AVStream *stream = _avFmtCtx->streams[index];
        AVCodecContext *codec = stream->codec;

        char infoCString[256];
        avcodec_string(infoCString, sizeof(infoCString), stream->codec, 1);
        NSString *strInfo = [NSString stringWithCString:infoCString encoding:NSUTF8StringEncoding];

         if (codec->codec_type == AVMEDIA_TYPE_AUDIO) {
             if (strInfo && [strInfo hasPrefix:@"Audio: "])
                 strInfo = [strInfo substringFromIndex:7];
             return strInfo;
         } else if (codec->codec_type == AVMEDIA_TYPE_VIDEO) {
             if (strInfo && [strInfo hasPrefix:@"Video: "])
                 strInfo = [strInfo substringFromIndex:7];
             return strInfo;
         } else {
             return @"";
         }
    }
    return @"";
}

- (void)updateStreamInfoWithSelectedStreamIndex:(int)index type:(int)mediaType {

    NSString *strInfo = [self codecInfoWithStreamIndex:index];
    if (mediaType == AVMEDIA_TYPE_AUDIO) {
        [_streamInfo setObject:strInfo forKey:STREAMINFO_KEY_AUDIO];
    } else if (mediaType == AVMEDIA_TYPE_VIDEO) {
        [_streamInfo setObject:strInfo forKey:STREAMINFO_KEY_VIDEO];
    } else {
        //do nothing for now..
    }
}

- (NSArray *)playableAudioStreams {
    NSMutableArray *array = [NSMutableArray array];

    for (int sIdx = 0; sIdx < _avFmtCtx->nb_streams; sIdx++) {

        AVStream *stream = _avFmtCtx->streams[sIdx];
        AVCodecContext *codec = stream->codec;

        if (codec->codec_type == AVMEDIA_TYPE_AUDIO && codec->channels > 0) {
            [array addObject:[NSNumber numberWithInt:sIdx]];
        }
    }
    return (NSArray *)array;
}

- (NSArray *)playableVideoStreams {
    NSMutableArray *array = [NSMutableArray array];

    for (int sIdx = 0; sIdx < _avFmtCtx->nb_streams; sIdx++) {

        AVStream *stream = _avFmtCtx->streams[sIdx];
        AVCodecContext *codec = stream->codec;

        if (codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            [array addObject:[NSNumber numberWithInt:sIdx]];
        }
    }
    return (NSArray *)array;
}

- (void)setVolumeLevel:(float)volumeLevel {
    if (_audioOk) {
        _volumeLevel = volumeLevel;
        [_aDecoder setVolumeLevel:_volumeLevel];
    }
}

- (void)setProbeSize:(int)probeSize {
    _probeSize = probeSize;
    if (_avFmtCtx) {
        _avFmtCtx->probesize = probeSize;
    }
}

- (void)setMaxAnalyzeDuration:(int)maxAnalyzeDuration {
    _maxAnalyzeDuration = maxAnalyzeDuration;
    if (_avFmtCtx) {
        _avFmtCtx->max_analyze_duration = _maxAnalyzeDuration;
    }
}

- (CGSize)naturalSize
{
    return CGSizeMake(_videoWidth, _videoHeight);
}

/*
- (BOOL)isVideoOk
{
    return _videoIsOK;
}

- (BOOL)isAudioOk
{
    return _audioIsOK;
}
 */

#pragma mark - Stream controllers

- (void)streamTogglePause
{
    if (_streamIsPaused) {
        [self setDecoderState:kDecoderStatePlaying errorCode:kRTErrorNone];
        if (_videoOk) {
            [_vDecoder onStreamPaused];
        }
    } else {
        [self setDecoderState:kDecoderStatePaused errorCode:kRTErrorNone];
    }
    [self updateExternalClockPts:[self externalClock]];
    _streamIsPaused = !_streamIsPaused;
}

/* seek in the stream */
- (void)streamSeek:(int64_t)pos rel:(int64_t)rel  byBytes:(int)byBytes
{
    if (!_seekRequest) {
        _seekPosition = pos;
        _seekRel = rel;
        _seekFlags &= ~AVSEEK_FLAG_BYTE;
        if (byBytes)
            _seekFlags |= AVSEEK_FLAG_BYTE;
        _seekRequest = 1;
    }
}

- (void)cycleStreamWithType:(int)type index:(int) sIndex {

    int start_index, stream_index;
    int old_index;
    AVStream *st;

    if (!_avFmtCtx || !_avFmtCtx->nb_streams)
        return;

    if (sIndex == -1) {
        //no index is given change audio stream to next if available
        if (type == AVMEDIA_TYPE_VIDEO) {
            start_index = _lastVideoStream;
            old_index = (int)[_vDecoder streamId];
        } else {
            start_index = _lastAudioStream;
            old_index = (int)[_aDecoder streamId];
        }

        stream_index = start_index;

        for (;;) {
            if (++stream_index >= _avFmtCtx->nb_streams)
            {
                if (start_index == -1)
                    return;
                stream_index = 0;
            }
            if (stream_index == start_index)
                return;

            st = _avFmtCtx->streams[stream_index];
            if (type == st->codec->codec_type) {
                /* check that parameters are OK */
                switch (type) {
                    case AVMEDIA_TYPE_AUDIO:
                        if (st->codec->sample_rate != 0 &&
                            st->codec->channels != 0)
                            goto the_end;
                        break;
                    case AVMEDIA_TYPE_VIDEO:
                        goto the_end;
                    default:
                        break;
                }
            }
        }
    } else {
        if (sIndex < _avFmtCtx->nb_streams) {
            stream_index = sIndex;
            st = _avFmtCtx->streams[stream_index];
            if (type == st->codec->codec_type) {
                /* check that parameters are OK */
                switch (type) {
                    case AVMEDIA_TYPE_AUDIO:
                        if (st->codec->sample_rate != 0 &&
                            st->codec->channels != 0)
                            goto the_end;
                        break;
                    case AVMEDIA_TYPE_VIDEO:
                        goto the_end;
                    default:
                        break;
                }
            }
        } else {
            return;
        }
    }

the_end:
    //stream_component_close(is, old_index);
    //stream_component_open(is, stream_index);

    if (type == AVMEDIA_TYPE_AUDIO) {
        [self closeAudioStream];
        RTError errAudio = kRTErrorNone;
        errAudio = [self openAudioStreamWithId:stream_index];

        if (errAudio == kRTErrorNone) {
            _audioOk = YES;
            _avFmtCtx->streams[stream_index]->discard = AVDISCARD_DEFAULT;
            [_vDecoder onAudioStreamCycled:_aDecoder];
            [self updateStreamInfoWithSelectedStreamIndex:stream_index type:AVMEDIA_TYPE_AUDIO];
            [_aDecoder performSelector:@selector(startAudioSystem)];
        }
    } else if (type == AVMEDIA_TYPE_VIDEO) {
        [self closeVideoStream];
        RTError errVideo = kRTErrorNone;
        errVideo = [self openVideoStreamWithId:stream_index audioIsOK:_audioOk];
        if (errVideo == kRTErrorNone) {
            _videoOk = YES;
            _avFmtCtx->streams[stream_index]->discard = AVDISCARD_DEFAULT;
            [self updateStreamInfoWithSelectedStreamIndex:stream_index type:AVMEDIA_TYPE_VIDEO];
        }
    }
    if (type == AVMEDIA_TYPE_VIDEO)
        _queueAttachmentsReq = 1;
}

- (void)setDecoderState:(DecoderState)state errorCode:(RTError)errCode {

    if (_decoderState != state) {
        _decoderState = state;
        dispatch_async(dispatch_get_main_queue(), ^(void) {
            if(_delegate && [_delegate respondsToSelector:@selector(decoderStateChanged:errorCode:)]) {
                [_delegate decoderStateChanged:state errorCode:errCode];
            }
            if (state == kDecoderStateConnected) {

            }
            
        });
    }
}

#pragma mark Decoder Utility methods

- (BOOL)isRealTime {
    if(!strcmp(_avFmtCtx->iformat->name, "rtp") ||
       !strcmp(_avFmtCtx->iformat->name, "rtsp") ||
       !strcmp(_avFmtCtx->iformat->name, "sdp"))
        return YES;

    if(_avFmtCtx->pb && (!strncmp(_avFmtCtx->filename, "rtp:", 4) ||
                         !strncmp(_avFmtCtx->filename, "udp:", 4)))
        return YES;

    return NO;
}

- (BOOL)isFileStream:(NSString *)fileName {
    if (fileName && fileName.length) {
        char *filename = (char *)[fileName UTF8String];
        char qolon = ':';//character to search
        char *found;

        found = strchr(filename, qolon);
        if (found) {
            size_t len = found - filename;
            if (len < 10) {
                return NO;
            }
        }
    }
    return YES;
}

#pragma mark - AudioSession interruption

#pragma mark iOS 5.x Audio interruption handling

- (void)beginInterruption {
    SLog(kLogLevelDecoder, @"Begin audio interuption");
    [self togglePause];
    if (_audioOk) {
        [_aDecoder stopUnit];
    }
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {
    // re-activate audio session after interruption
    NSError *error;
    if(![[AVAudioSession sharedInstance] setActive:YES error:&error]) {
        SLog(kLogLevelDecoder, @"Error: Audio Session could not be activated: %@", error);
    }
    SLog(kLogLevelDecoder, @"End Audio interuption");
    if (_audioOk) {
        
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 70000
        if ([[[UIDevice currentDevice] systemVersion] compare:@"7.0" options:NSNumericSearch] != NSOrderedAscending) {
            //running on iOS 7.0 or higher
            [_aDecoder performSelector:@selector(startUnit) withObject:nil afterDelay:2.0];
        } else {
            [_aDecoder startUnit];
        }
#else
        [_aDecoder startUnit];
#endif
        
    }
    [self togglePause];
}

#pragma mark iOS 6.x or higher Audio interruption handling

- (void) interruption:(NSNotification*)notification
{
    NSDictionary *interuptionDict = notification.userInfo;
    NSUInteger interuptionType = (NSUInteger)[[interuptionDict valueForKey:AVAudioSessionInterruptionTypeKey] integerValue];
    
    if (interuptionType == AVAudioSessionInterruptionTypeBegan) {
        [self beginInterruption];
    } else if (interuptionType == AVAudioSessionInterruptionTypeEnded) {
        [self endInterruptionWithFlags:0];
    }
}


#pragma mark - Application callbacks

- (void)appDidEnterBackground {
    _appIsInBackgroundNow = YES;
    SLog(kLogLevelDecoder, @"Application did enter background now...");
}

- (void)appWillEnterForeground {
    _appIsInBackgroundNow = NO;
    SLog(kLogLevelDecoder, @"Application will enter foreground now...");
}

#pragma mark - Reachability callback
- (void)onReachabilityChanged:(NSNotification *)notification {
    RTNetworkStatus status = [_reachability currentReachabilityStatus];

    if (!_streamInfo)
        return;

    if(status == kRTNetworkStatusReachableViaWiFi) {
        [_streamInfo setObject:TR(@"Wifi") forKey:STREAMINFO_KEY_CONNECTION];
    } else if (status == kRTNetworkStatusReachableViaWWAN) {
        [_streamInfo setObject:TR(@"3G/Edge") forKey:STREAMINFO_KEY_CONNECTION];
    }else if (status == kRTNetworkStatusNotReachable) {
        [_streamInfo setObject:TR(@"None") forKey:STREAMINFO_KEY_CONNECTION];
    }
}

#pragma mark - Shutdown

- (void)shutDown {
    if (_audioOk) {
        if (_vDecoder){
            [_vDecoder onAudioDecoderDestroyed];
        }
        [self closeAudioStream];
    }
    if (_videoOk) {
        [self closeVideoStream];
    }
}

- (void)destroyReachability {
    [_reachability stopNotifier];
    [_reachability release];
    _reachability = nil;
}

- (void)destroyDispatchQueues {
    dispatch_release(_readPktQueue);
}

- (void)dealloc {
    
    if (_avFmtCtx) {
        _avFmtCtx->interrupt_callback.callback = NULL;
        _avFmtCtx->interrupt_callback.opaque = NULL;
        avformat_close_input(&_avFmtCtx);
        _avFmtCtx = NULL;
        SLog(kLogLevelDecoder, @"avformat_close_input");
    }

    [self destroyReachability];
    [self destroyDispatchQueues];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_streamInfo release];
    [_decodeOptions release];
    [_streamURL release];

    avformat_network_deinit();

    SLog(kLogLevelDecoder, @"Decoder manager is deallocated...");
    [super dealloc];
}

@end

#pragma mark - decoder interrupt function

static int decode_interrupt_cb(void *decoder)
{
    if (decoder)
        return [(DecodeManager *)decoder abortIsRequested];
    return 0;
}
