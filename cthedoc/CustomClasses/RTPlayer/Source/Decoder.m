
#import "Decoder.h"
#import "Packet.h"

@interface Decoder() {

}

@end

@implementation Decoder

@synthesize pktQueueSize = _pktQueueSize;
@synthesize pktQueue = _pktQueue;
@synthesize streamId = _streamId;
@synthesize manager = _manager;
@synthesize abortRequest = _abortRequest;

#pragma mark - Initialization

- (id)initWithCodecContext:(AVCodecContext*)cdcCtx stream:(AVStream *)strm streamId:(NSInteger)sId manager:(id)manager {
    self = [super init];
    if (self) {
        _manager = manager;
        _codecContext = cdcCtx;
        _stream = strm;
        _streamId = sId;

        _abortRequest = 0;
        _queueSerial = 0;
        
        [self initPktMutex];
        [self createPktQueue];
        [self createFlushPkt];
    }
    return self;
}

- (void)initPktMutex {
    pthread_mutex_init(&_mutexPkt, NULL);
    pthread_cond_init(&_condPkt, NULL);
}

- (void)createPktQueue {
    _pktQueue = [[NSMutableArray alloc] init];
    _pktQueueSize = 0;
}

- (void)createFlushPkt {
    av_init_packet(&_flushPkt);
    _flushPkt.data = (uint8_t *)(intptr_t)"FLUSH";
    _flushPkt.size = (int)strlen((const char *)_flushPkt.data);
}


#pragma mark - Mutex & condition for managing packets processing priority

- (pthread_mutex_t*)mutexPkt {
    return &_mutexPkt;
}

- (pthread_cond_t*)condPkt {
    return &_condPkt;
}

#pragma mark - Actions

- (void)addFlushPkt {
    [self addPacket:&_flushPkt];
}

- (void)addPacket:(AVPacket*)packet {

    if (_abortRequest) {
        return;
    }

    BOOL isFlushPkt = NO;
    if (packet == &_flushPkt){
        _queueSerial++;
        isFlushPkt = YES;
    }

    _pktQueueSize = _pktQueueSize + packet->size;
    Packet *streamPkt = [[Packet alloc] initWithPkt:packet serial:_queueSerial isFlush:isFlushPkt];
    [_pktQueue insertObject:streamPkt atIndex:0];
    [streamPkt release];
}

#pragma mark - Shutdown

- (void)clearPktQueue {
    if ([_pktQueue count]) {
        [_pktQueue removeAllObjects];
        _pktQueueSize = 0;
    }
}

- (void)unlockQueues {
    pthread_mutex_lock(&_mutexPkt);
    pthread_cond_signal(&_condPkt);
    pthread_mutex_unlock(&_mutexPkt);
}

- (void)dealloc {

    pthread_cond_destroy(&_condPkt);
    pthread_mutex_destroy(&_mutexPkt);
    [_pktQueue release];
    [super dealloc];
}


@end
