
#import "VideoDecoder.h"
#import "DecodeManager.h"
#import "AudioDecoder.h"
#import "VideoFrame.h"
#import "Packet.h"

#include "libavutil/time.h"
#include "libswscale/swscale.h"

//Settings
static int decoder_reorder_pts = -1; /* 0=off 1=on -1=auto */

//Defines
/* polls for possible required screen refresh at least this often, should be less than 1/fps */
#define REFRESH_RATE 0.01

static void fillFrameData(UInt8 *src, UInt8 *dst, int size, int linesize, int width, int height)
{
    width = MIN(linesize, width);
    memset(dst, 0, size);
    for (NSUInteger i = 0; i < height; ++i) {
        memcpy(dst, src, width);
        dst += width;
        src += linesize;
    }
}

@interface VideoDecoder() {

    AVFormatContext* _avFmtCtx; /* avformat context reference */
    NSMutableArray *_pictQueue; /* picture (ready to display) queue */
    
    struct SwsContext *_imgConvertCtx; /* image format convertor, uses CPU */
    AVPicture _picture; /* */
    VideoStreamColorFormat _colorFormat;

    //mutex & condition
    pthread_mutex_t _mutexPictQueue; /* mutex for managing threads */
    pthread_cond_t _condPictQueue; /* condition for managing threads */

    //dispatch queues
    dispatch_queue_t _videoDecodeQueue;
    dispatch_queue_t _pictActionQueue;

    BOOL _decodeJobIsDone;
    BOOL _schedulePictureJobIsDone;

    BOOL _refreshInProgress;

    AudioDecoder *_audioDecoder; /* if audio works, then we will sync a-v due to audio */

    //frame related vars
    double _frameTimer;
    double _frameLastPts;
    int64_t _frameLastDroppedPos;
    double _frameLastDuration;
    double _frameLastDroppedPts;
    double _frameLastFilterDelay;
    double _currentPts; /* current displayed pts (different from video_clock if frame fifos are used) */
    double _currentPtsDrift;
    int _frameDropsEarly;
    int _frameDropsLate;
    int64_t _currentPos;
    double _frameLastDelay;
    double _clock; /* pts of last decoded frame / predicted pts of next */

    //picture related vars
    int _pictQSize, _pictQRIndex, _pictQWIndex;


    //settings
    int _allowFrameDrop; /* drop frames when cpu is too slow */

    int _clockSerial;
    int _forceRefresh;
    BOOL _eof;
    double _remainingTime;

    dispatch_semaphore_t _semaSchedulePicThread;
    dispatch_semaphore_t _semaDecodeThread;
}

- (double) videoClock; /* get the current video clock value for av sync issue */

@end


@implementation VideoDecoder

@synthesize decodeJobIsDone = _decodeJobIsDone;
@synthesize schedulePictureJobIsDone = _schedulePictureJobIsDone;
@synthesize currentPos = _currentPos;

#pragma mark - Initialization

- (id)initWithFormatContext:(AVFormatContext*)avFmtCtx codecContext:(AVCodecContext*)cdcCtx stream:(AVStream *)strm
                   streamId:(NSInteger)sId manager:(id)manager audioDecoder:(AudioDecoder *)audioDecoder {
    self = [super initWithCodecContext:cdcCtx stream:strm streamId:sId manager:manager];
    if (self) {
        [self initMutex];
        [self createFrameBuffers];
        [self createDispatchQueues];
        [self initValues];
        [self initSettings];
        _audioDecoder = audioDecoder;
    }
    return self;
}

- (void)initMutex {
    pthread_mutex_init(&_mutexPictQueue, NULL);
    pthread_cond_init(&_condPictQueue, NULL);
}

- (void)createFrameBuffers {
    _pictQueue = [[NSMutableArray alloc] init];
    
    _colorFormat = [(DecodeManager *)_manager videoStreamColorFormat];
    if (_colorFormat != VideoStreamColorFormatYUV) {
        avpicture_alloc(&_picture, PIX_FMT_RGBA, _codecContext->width, _codecContext->height);
    }
    
    for (int i = 0; i < [(DecodeManager *)_manager videoPictureQueueSize]; i++) {
        
        VideoFrame *vidFrame = [[VideoFrame alloc] initWithColorFormat:_colorFormat];

        int w = _codecContext->width;
        int h = _codecContext->height;
        int size = w*h;

        if (_colorFormat == VideoStreamColorFormatYUV) {
            vidFrame.pLuma.size = size;
            vidFrame.pLuma.data = (UInt8*)malloc(size);
            memset(vidFrame.pLuma.data, 0, vidFrame.pLuma.size);
            
            vidFrame.pChromaB.size = size/2;
            vidFrame.pChromaB.data = (UInt8*)malloc(size/2);
            memset(vidFrame.pChromaB.data, 0, vidFrame.pChromaB.size);
            
            vidFrame.pChromaR.size = size/2;
            vidFrame.pChromaR.data = (UInt8*)malloc(size/2);
            memset(vidFrame.pChromaR.data, 0, vidFrame.pChromaR.size);
        } else {
            vidFrame.pRGB.size = _picture.linesize[0]*h;
            vidFrame.pRGB.data = (UInt8*)malloc(vidFrame.pRGB.size);
            memset(vidFrame.pRGB.data, 0, vidFrame.pRGB.size);
        }

        [_pictQueue addObject:vidFrame];
        [vidFrame release];
    }
}

- (void)createDispatchQueues {
    _pictActionQueue = dispatch_queue_create("picture_schedule_and_refresh_queue", NULL);
    _videoDecodeQueue = dispatch_queue_create("frame_decode_queue", NULL);
}

- (void)initValues {
    _decodeJobIsDone = YES;
    _schedulePictureJobIsDone = YES;

    _refreshInProgress = NO;

    _frameTimer = 0;
    _currentPos = 0;
    _frameLastPts = 0;
    _currentPts = 0;
    _currentPtsDrift = (_audioDecoder) ? [_audioDecoder currentPtsDrift] : 0;
    _frameLastDuration = 0;
    _frameLastDroppedPts = 0;
    _frameLastFilterDelay = 0;
    _frameLastDroppedPos = 0;
    _frameDropsEarly = 0;
    _frameDropsLate = 0;

    _pictQSize = _pictQRIndex = _pictQWIndex = 0;

    _clockSerial = -1;
    _remainingTime = 0.0;
    _eof = NO;
}

- (void)initSettings {
    _allowFrameDrop = -1;
}


#pragma mark - Actions

#pragma mark Decode video frames to pictures

- (int)decodeVideo {

    _semaDecodeThread = dispatch_semaphore_create(0);

    dispatch_async(_videoDecodeQueue, ^(void) {
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

        AVPacket packet;
        int gotPicture;
        __block Packet *vidPkt;
        _decodeJobIsDone = NO;

        AVFrame *frame = avcodec_alloc_frame();
        int64_t ptsInt = AV_NOPTS_VALUE, pos = -1;
        __block double pts;
        int pktSerial = 0;

        for (;;) {
            int retValue = 0;

            while (([(DecodeManager *)_manager streamIsPaused] ||
                    (_audioDecoder && [_audioDecoder isWaitingForPackets] &&
                     [(DecodeManager *)_manager remoteFileStreaming])) && !_abortRequest)
                [NSThread sleepForTimeInterval:0.01];

            if (_abortRequest) {
                break;
            }

            /* try to get a packet, if successful decode it */
            BOOL isPktFlush = NO;

            pthread_mutex_lock(&_mutexPkt);
            int totalCount = (int)[_pktQueue count];
            if (!totalCount) {
                pthread_cond_wait(&_condPkt, &_mutexPkt);
            }

            vidPkt = [(Packet *)[_pktQueue lastObject] retain];
            if (!vidPkt) {
                _pktQueueSize = _pktQueueSize - vidPkt.size;
                [_pktQueue removeLastObject];
                [vidPkt release];
            }

            if (vidPkt.flush) {
                isPktFlush = YES;
                _pktQueueSize = _pktQueueSize - vidPkt.size;
                [_pktQueue removeLastObject];
                [vidPkt release];
                avcodec_flush_buffers(_codecContext);
            }
            pthread_mutex_unlock(&_mutexPkt);

            if (isPktFlush) {
                pthread_mutex_lock(&_mutexPictQueue);
                while (_pictQSize && !_abortRequest) {
                    pthread_cond_wait(&_condPictQueue, &_mutexPictQueue);
                }
                _currentPos = -1;
                _frameLastPts = AV_NOPTS_VALUE;
                _frameLastDuration = 0;
                _frameTimer = (double)av_gettime() / 1000000.0;
                _frameLastDroppedPts = AV_NOPTS_VALUE;
                pthread_mutex_unlock(&_mutexPictQueue);
                continue;
            }

            av_init_packet(&packet);
            packet.data = (uint8_t *)vidPkt.samples;
            packet.size = vidPkt.size;
            packet.pts = vidPkt.pts;
            packet.dts = vidPkt.dts;
            pktSerial = vidPkt.serial;

            int decodeCode = avcodec_decode_video2(_codecContext, frame, &gotPicture, &packet);

            if ((decodeCode > 0) && gotPicture) {
                int ret = 1;

                if (decoder_reorder_pts == -1) {
                    ptsInt = av_frame_get_best_effort_timestamp(frame);
                } else if (decoder_reorder_pts) {
                    ptsInt = frame->pkt_pts;
                } else {
                    ptsInt = frame->pkt_dts;
                }

                if (ptsInt == AV_NOPTS_VALUE) {
                    ptsInt = 0;
                }

                if (((_audioDecoder)) &&
                    (_allowFrameDrop >0 || (_allowFrameDrop &&
                                            [(DecodeManager *)_manager masterSyncType] != AV_SYNC_VIDEO_MASTER))) {

                    pthread_mutex_lock(&_mutexPictQueue);

                    if (_frameLastPts != AV_NOPTS_VALUE && ptsInt) {
                        double clockdiff = [self videoClock] - [(DecodeManager *)_manager masterClock];
                        double dpts = av_q2d(_stream->time_base) * ptsInt;
                        double ptsdiff = dpts - _frameLastPts;

                        if (!isnan(clockdiff) && fabs(clockdiff) < AV_NOSYNC_THRESHOLD &&
                            ptsdiff > 0 && ptsdiff < AV_NOSYNC_THRESHOLD &&
                            clockdiff + ptsdiff - _frameLastFilterDelay < 0) {
                            _frameLastDroppedPos = packet.pos;
                            _frameLastDroppedPts = dpts;
                            _frameDropsEarly++;
                            ret = 0;
                        }
                    }
                    pthread_mutex_unlock(&_mutexPictQueue);
                }
                retValue = ret;
            }

            pos = packet.pos;

            if (retValue == 0) {
                pthread_mutex_lock(&_mutexPkt);
                if (pktSerial == _queueSerial) {
                    _pktQueueSize = _pktQueueSize - packet.size;
                    [_pktQueue removeLastObject];
                }
                [vidPkt release];
                pthread_mutex_unlock(&_mutexPkt);
                continue;
            }

            if (retValue < 0) {
                pthread_mutex_lock(&_mutexPkt);
                if (pktSerial == _queueSerial) {
                    _pktQueueSize = _pktQueueSize - packet.size;
                    [_pktQueue removeLastObject];
                }
                [vidPkt release];
                pthread_mutex_unlock(&_mutexPkt);
                goto the_end;
            }


            pts = ptsInt * av_q2d(_stream->time_base);

            retValue = [self queuePictureWithFrame:frame withPts:pts withPos:pos withSerial:pktSerial];

            if (retValue < 0) {
                pthread_mutex_lock(&_mutexPkt);
                if (pktSerial == _queueSerial) {
                    _pktQueueSize = _pktQueueSize - packet.size;
                    [_pktQueue removeLastObject];
                }
                [vidPkt release];
                pthread_mutex_unlock(&_mutexPkt);
                goto the_end;
            }

            pthread_mutex_lock(&_mutexPkt);
            NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
            if (pktSerial == _queueSerial) {
                _pktQueueSize = _pktQueueSize - packet.size;
                [_pktQueue removeLastObject];
            }
            [vidPkt release];
            [pool release];
            pthread_mutex_unlock(&_mutexPkt);
        }

    the_end:

        if (!_abortRequest) {
            avcodec_flush_buffers(_stream->codec);
        }

        av_free(frame);

        SLog(kLogLevelDecoder, @"decodeVideo is ENDED!");
        _decodeJobIsDone = YES;

        dispatch_semaphore_signal(_semaDecodeThread);

        [pool release];
    });

    return 0;
}

- (int)queuePictureWithFrame:(AVFrame *) pFrame withPts:(double) pts1
                     withPos:(int64_t) pos withSerial:(int) frameSerial {

    pthread_mutex_lock(&_mutexPictQueue);
    while (_pictQSize >= [(DecodeManager *)_manager videoPictureQueueSize] -2 &&
           !_abortRequest) {
        pthread_cond_wait(&_condPictQueue, &_mutexPictQueue);
    }
    pthread_mutex_unlock(&_mutexPictQueue);

    if (_abortRequest)
        return -1;

    VideoFrame *vidFrame = [_pictQueue objectAtIndex:_pictQWIndex];
    VideoStreamColorFormat colorFormat = [(DecodeManager *)_manager videoStreamColorFormat];
    
    if (colorFormat == VideoStreamColorFormatYUV) {
        fillFrameData(pFrame->data[0], vidFrame.pLuma.data, vidFrame.pLuma.size, pFrame->linesize[0],
                      _codecContext->width, _codecContext->height);
        fillFrameData(pFrame->data[1], vidFrame.pChromaB.data, vidFrame.pChromaB.size, pFrame->linesize[1],
                      _codecContext->width/2, _codecContext->height/2);
        fillFrameData(pFrame->data[2], vidFrame.pChromaR.data, vidFrame.pChromaR.size, pFrame->linesize[2],
                      _codecContext->width/2, _codecContext->height/2);
    } else {
        if (!_imgConvertCtx) {
            _imgConvertCtx = sws_getCachedContext(_imgConvertCtx, _codecContext->width, _codecContext->height,
                                               _codecContext->pix_fmt, _codecContext->width, _codecContext->height,
                                               PIX_FMT_RGBA, SWS_FAST_BILINEAR, NULL, NULL, NULL);
        }
        sws_scale(_imgConvertCtx, (const uint8_t **)pFrame->data, pFrame->linesize,
                      0, _codecContext->height, _picture.data, _picture.linesize);
        memcpy(vidFrame.pRGB.data, _picture.data[0], _picture.linesize[0] * _codecContext->height);
    }

    vidFrame.width = _codecContext->width;
    vidFrame.height = _codecContext->height;
    vidFrame.pts = pts1;
    vidFrame.pos = pos;
    vidFrame.serial = frameSerial;

    AVRational ratio = av_guess_sample_aspect_ratio(_avFmtCtx , _stream, pFrame);
    if (ratio.num == 0)
        vidFrame.aspectRatio = 0;
    else
        vidFrame.aspectRatio = av_q2d(ratio);

    if (vidFrame.aspectRatio <= 0.0)
        vidFrame.aspectRatio = 1.0;

    _pictQWIndex++;
    if (_pictQWIndex == [(DecodeManager *)_manager videoPictureQueueSize]){
        _pictQWIndex = 0;
    }
    pthread_mutex_lock(&_mutexPictQueue);
    _pictQSize++;
    pthread_mutex_unlock(&_mutexPictQueue);

    return 0;
}

#pragma mark Setting picture ready

- (void) schedulePicture {

    _semaSchedulePicThread = dispatch_semaphore_create(0);
    dispatch_async(_pictActionQueue, ^(void) {
        _schedulePictureJobIsDone = NO;
        NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
        while (!_abortRequest) {
            if (_remainingTime > 0.0)
                av_usleep((unsigned int)(_remainingTime * 1000000.0));
            _remainingTime = REFRESH_RATE;
            if (!_refreshInProgress && (_forceRefresh ||
                                        ![(DecodeManager *)_manager remoteFileStreaming] ||
                                        !([(DecodeManager *)_manager streamIsPaused] ||
                                          (_audioDecoder && [_audioDecoder isWaitingForPackets])))) {
                _refreshInProgress = YES;
                [self performSelector:@selector(refreshPicture)];
            }
        }

        _schedulePictureJobIsDone = YES;
        SLog(kLogLevelDecoder, @"schedulePicture is ENDED");

        dispatch_semaphore_signal(_semaSchedulePicThread);
        [pool release];
    });

}

- (void) refreshPicture {

    double time;

    if (![(DecodeManager *)_manager streamIsPaused] &&
        [(DecodeManager *)_manager masterSyncType] == AV_SYNC_EXTERNAL_CLOCK &&
        [(DecodeManager *)_manager isRealTime]) {
        [(DecodeManager *)_manager checkExternalClockSpeed];
    }

    if (_stream) {

        int redisplay = 0;
        if (_forceRefresh)
            redisplay = [self pictqPrevPicture];

    retry:

        if (_abortRequest) {
            return;
        }

        if (_pictQSize == 0) {
            pthread_mutex_lock(&_mutexPictQueue);
            if (_frameLastDroppedPts != AV_NOPTS_VALUE && _frameLastDroppedPts > _frameLastPts) {
                [self updateVideoWithPts:_frameLastDroppedPts withPos:_frameLastDroppedPos withSerial:0];
                _frameLastDroppedPts = AV_NOPTS_VALUE;
            }
            pthread_mutex_unlock(&_mutexPictQueue);

            // nothing to do, no picture to display in the queue
        } else {
            double lastDuration, duration, delay;
            duration = 0.0;
            VideoFrame *vidFrame;

            vidFrame = [_pictQueue objectAtIndex:_pictQRIndex];

            if (vidFrame.serial != _queueSerial) {
                [self pictqNextPicture];
                redisplay = 0;
                goto retry;
            }

            if (!_abortRequest && [(DecodeManager *)_manager streamIsPaused]){
                goto display;
            }

            /* compute nominal last_duration */
            lastDuration = vidFrame.pts - _frameLastPts;
            if (lastDuration > 0 && lastDuration < [(DecodeManager *)_manager maxFrameDuration]) {
                /* if duration of the last frame was sane, update last_duration in video state */
                _frameLastDuration = lastDuration;
            }
            delay = [self computeTargetDelayWithVal:_frameLastDuration];

            time = av_gettime()/1000000.0;

            if (time < (_frameTimer + delay)) {
                _remainingTime = FFMIN(_frameTimer + delay - time, _remainingTime);
                _refreshInProgress = NO;
                return;
            }

            if (delay > 0)
                _frameTimer += delay * FFMAX(1, floor((time - _frameTimer) / delay));

            pthread_mutex_lock(&_mutexPictQueue);
            [self updateVideoWithPts:vidFrame.pts withPos:vidFrame.pos withSerial:vidFrame.serial];
            pthread_mutex_unlock(&_mutexPictQueue);

            if (_pictQSize > 1) {
                VideoFrame *nextVidFrame = [_pictQueue objectAtIndex:((_pictQRIndex + 1) % [(DecodeManager *)_manager videoPictureQueueSize])];
                duration = nextVidFrame.pts - vidFrame.pts; // More accurate this way, 1/time_base is often not reflecting FPS

                if (_audioDecoder) {
                    if(![(DecodeManager *)_manager step] && (redisplay || _allowFrameDrop > 0 || (_allowFrameDrop && [(DecodeManager *)_manager masterSyncType] != AV_SYNC_VIDEO_MASTER)) && time > _frameTimer + duration){
                        if (!redisplay)
                            _frameDropsLate++;
                        [self pictqNextPicture];
                        redisplay = 0;
                        goto retry;
                    }
                }
            }

            if((_allowFrameDrop > 0 || (_allowFrameDrop && _audioDecoder)) && (time > (_frameTimer + duration))){
                if(_pictQSize > 1){
                    _frameDropsLate++;
                    [self pictqNextPicture];
                    goto retry;
                }
            }

        display:
            /* display picture */
            if (!_abortRequest){
                if (![(DecodeManager *)_manager appIsInBackgroundNow]) {
                    [self performSelectorOnMainThread:@selector(displayPicture) withObject:nil waitUntilDone:YES];
                } else {
                    [NSThread sleepForTimeInterval:REFRESH_RATE];
                }
                [self pictqNextPicture];
            }
            if ([(DecodeManager *)_manager step] && ![(DecodeManager *)_manager streamIsPaused])
                [(DecodeManager *)_manager streamTogglePause];

        }
    }

    _forceRefresh = 0;

    if (!_eof) {
        static int logIter = 0;
        int modSync = 1/[(DecodeManager *)_manager avSyncLogFrequency];

        if((log_level & kLogLevelAVSync) && (logIter%modSync == 0)){
            logIter = 0;
            static int64_t last_time;
            int64_t cur_time;
            double av_diff;

            cur_time = av_gettime();
            if (!last_time || (cur_time - last_time) >= 30000) {

                av_diff = 0;
                if (_audioDecoder && _stream)
                    av_diff = [(DecodeManager *)_manager clockDifference];

                printf("A-V:%7.3f , fd_early=%4d , fd_late=%4d\n",
                       av_diff, _frameDropsEarly, _frameDropsLate);
                
                fflush(stdout);
                last_time = cur_time;
            }
        }
        logIter++;
    }
    _refreshInProgress = NO;
}

- (void) displayPicture {
    VideoFrame *vidFrame;
    vidFrame = [_pictQueue objectAtIndex:_pictQRIndex];
    
    if (!vidFrame)
        return;
    
    if ((_colorFormat == VideoStreamColorFormatYUV) &&
        (vidFrame.pLuma.size != vidFrame.width * vidFrame.height)) {
        return;
    }
    
    if (!(_abortRequest || [(DecodeManager *)_manager appIsInBackgroundNow])) {
        [[[(DecodeManager *)_manager delegate] performSelector:NSSelectorFromString(@"renderView")] performSelector:NSSelectorFromString(@"updateScreenWithFrame:") withObject: vidFrame];
    }
}

#pragma mark AV syncing

- (double) computeTargetDelayWithVal:(double) delay
{
    double syncThreshold, diff;

    /* update delay to follow master synchronisation source */
    if ([(DecodeManager *)_manager masterSyncType] != AV_SYNC_VIDEO_MASTER) {
        /* if video is slave, we try to correct big delays by
         duplicating or deleting a frame */
        diff = [self videoClock] - [(DecodeManager *)_manager masterClock];

        /* skip or repeat frame. We take into account the
         delay to compute the threshold. */
        syncThreshold = FFMAX(AV_SYNC_THRESHOLD, delay);
        if (!isnan(diff) && fabs(diff) < AV_NOSYNC_THRESHOLD) {
            if (diff <= -syncThreshold)
                delay = 0;
            else if (diff >= syncThreshold)
                delay = 2 * delay;
        }
    }
    return delay;
}

-(int)pictqPrevPicture {

    VideoFrame *prevVidFrame;
    int ret = 0;
    int picQueueSizeDefined = [(DecodeManager *)_manager videoPictureQueueSize];
    int prevIndex = ((_pictQRIndex + picQueueSizeDefined -1) % picQueueSizeDefined);
    prevVidFrame = [_pictQueue objectAtIndex:prevIndex];

    if (prevVidFrame && prevVidFrame.serial == _queueSerial) {
        pthread_mutex_lock(&_mutexPictQueue);

        if (_pictQSize < (picQueueSizeDefined - 1)) {
            if ((_pictQRIndex - 1) == -1)
                _pictQRIndex = picQueueSizeDefined - 1;
            _pictQSize++;
            ret = 1;
        }
        pthread_cond_signal(&_condPictQueue);
        pthread_mutex_unlock(&_mutexPictQueue);
    }
    return ret;
}

- (void) pictqNextPicture {
    /* update queue size and signal for next picture */
    _pictQRIndex++;
    if (_pictQRIndex == [(DecodeManager *)_manager videoPictureQueueSize]) {
        _pictQRIndex = 0;
    }

    pthread_mutex_lock(&_mutexPictQueue);
    _pictQSize--;
    pthread_cond_signal(&_condPictQueue);
    pthread_mutex_unlock(&_mutexPictQueue);
}

-(void) updateVideoWithPts:(double) pts withPos:(int64_t) pos withSerial:(int) pktSerial {
    double time = av_gettime() / 1000000.0;
    /* update current video pts */
    _currentPts = pts;
    _currentPtsDrift = _currentPts - time;
    _currentPos = pos;
    _frameLastPts = pts;

    _clockSerial = pktSerial;
    if (pktSerial == _queueSerial) {
        [(DecodeManager *)_manager checkExternalClockSync:_currentPts];
    }
}

- (double)videoClock
{
    if (_clockSerial != _queueSerial)
        return NAN;
    if ([(DecodeManager *)_manager streamIsPaused]) {
        return _currentPts;
    }
    return _currentPtsDrift + (av_gettime() / 1000000.0);
}

#pragma mark - On State change actions

- (void)onStreamPaused {
    _frameTimer += av_gettime() / 1000000.0 + _currentPtsDrift - _currentPts;
    if ([(DecodeManager *)_manager readPauseCode] != AVERROR(ENOSYS)) {
        _currentPts = _currentPtsDrift + av_gettime() / 1000000.0;
    }
    _currentPtsDrift = _currentPts - av_gettime() / 1000000.0;
}

- (void)onAudioStreamCycled:(AudioDecoder *)decoder {
    if (decoder) {
        _audioDecoder = decoder;
    }
}

- (void)onAudioDecoderDestroyed {
    _audioDecoder = NULL;
}

- (void)setEOF:(BOOL)value {
    _eof = value;
}

#pragma mark - Shutdown

- (void)shutdown {

    [self unlockQueues];

    if (_semaSchedulePicThread) {
        dispatch_semaphore_wait(_semaSchedulePicThread, DISPATCH_TIME_FOREVER);
        dispatch_release(_semaSchedulePicThread);
        _semaSchedulePicThread = NULL;
    }

    if (_semaDecodeThread) {
        dispatch_semaphore_wait(_semaDecodeThread, DISPATCH_TIME_FOREVER);
        dispatch_release(_semaDecodeThread);
        _semaDecodeThread = NULL;
    }

    if (_streamId >= 0) {
		_stream->discard = AVDISCARD_ALL;
	}
    
    if (_codecContext) {
        avcodec_close(_codecContext);
    }
    [self clearPktQueue];
}

- (void)unlockQueues {
    pthread_mutex_lock(&_mutexPictQueue);
    _pictQSize--;
    pthread_cond_signal(&_condPictQueue);
    pthread_mutex_unlock(&_mutexPictQueue);
    
    [super unlockQueues];
}

- (void)dealloc {
    SLog(kLogLevelDecoder, @"Video Decoder is deallocated...");
    
    if (_imgConvertCtx) {
        sws_freeContext(_imgConvertCtx);
    }
    avpicture_free(&_picture);
    
    pthread_cond_destroy(&_condPictQueue);
    pthread_mutex_destroy(&_mutexPictQueue);
    
    [_pictQueue removeAllObjects];
    [_pictQueue release];
    
    dispatch_release(_videoDecodeQueue);
    dispatch_release(_pictActionQueue);
    
    [super dealloc];
}

@end
