//
//  NSString+ContainString.m
//  WizSupportDemo
//
//  Created by Yossi on 16/06/15.
//  Copyright (c) 2015 Yossi. All rights reserved.
//

#import "NSString+ContainString.h"

@implementation NSString (ContainString)

- (BOOL)myContainsString:(NSString*)other {
    NSRange range = [self rangeOfString:other];
    return range.length != 0;
}

@end
