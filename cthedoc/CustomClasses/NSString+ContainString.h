//
//  NSString+ContainString.h
//  WizSupportDemo
//
//  Created by Yossi on 16/06/15.
//  Copyright (c) 2015 Yossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ContainString)

- (BOOL)myContainsString:(NSString*)other;

@end
