//
//  ChatViewController.m
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import "ChatViewController.h"
#import "ChatMessage.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"
#import "WebChatViewController.h"


// Spaces
#define kMessageToMessage       8
#define kWithIconToMessage      20
#define kWithIconToUserMessage  25
#define kMessageMargin          18

@interface ChatViewController ()

@end

@implementation ChatViewController
{
    NSTimer* callStatusTimer;
    BOOL goToVideo;
    BOOL goToSnapper;
    NSString* webUrl;
    
    // chat
    CGFloat  lastBubbleY;
    CGRect lastBubbleFrame;
    
    
    ChatMessageViewController *loadingMessage;
    
    NSInteger lastBubbleIndex;
    
    CGRect keyboardFrame;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    
    [self.backBtn setImage:[UIImage new]];
    [self.backBtn setTitle:@""];
    
    MBProgressHUD *progress = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    progress.mode = MBProgressHUDModeIndeterminate;
    progress.labelText = @"Waiting for an agent";
    
    self.messages = @[];
    // Init last y
    lastBubbleY = 20;
    // Clean chat
    [self.contentScroll.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    

    // Set chat background
    self.contentScroll.backgroundColor = [UIColor clearColor];
    
    self.contentScroll.canCancelContentTouches = NO;
    self.contentScroll.delaysContentTouches = NO;
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnScroll)];
    tap.numberOfTapsRequired = 1;
    tap.cancelsTouchesInView = NO;
    [self.contentScroll addGestureRecognizer:tap];
    
}


-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    
    self.contentScroll.contentSize = self.contentScroll.frame.size;
    
    lastBubbleIndex = self.messages.count;
    
    goToVideo = NO;
    goToSnapper=NO;
    [self setGetCallContentTimer];
    
    
    if (lastBubbleY> self.contentScroll.contentSize.height) {
        CGSize size = self.contentScroll.contentSize;
        size.height+= (lastBubbleY - size.height) + 20 ;
        self.contentScroll.contentSize = size;
        
        CGPoint bottomOffset = CGPointMake(0, self.contentScroll.contentSize.height - self.contentScroll.bounds.size.height);
        [self.contentScroll setContentOffset:bottomOffset animated:YES];
    }
    
    self.sendBtn.enabled = (self.messageTextView.text.length>0);
}

-(void) viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [callStatusTimer invalidate];
    
    [[NSNotificationCenter defaultCenter]  removeObserver:self
                  name:UIKeyboardWillShowNotification
                object:nil];
    [[NSNotificationCenter defaultCenter]  removeObserver:self
                  name:UIKeyboardWillHideNotification
                object:nil];

    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewWillLayoutSubviews{
    self.sendBtn.layer.cornerRadius = 5;
    self.sendBtn.clipsToBounds = YES;
}

#pragma -mark CHAT MESSAGES

-(void) tappedOnScroll{
    [self.messageTextView resignFirstResponder];
}

-(void) initChatMessageBubble:(ChatMessage*)message tag:(NSInteger)tag {
    ChatMessageType type = kMessageTypeOper;
    if (!message.isOper) {
        type = kMessageTypeClient;
    }
    
    NSString* text = @"";
    if (message.isHtml) {
        NSArray* arr  = [message.text componentsSeparatedByString:@"'"];

        if (arr.count>9) {
            if (arr.count>16) {
                return;
            }
            else
                text = [arr objectAtIndex:9];
        }
    }
    else{
        text = message.text;
    }
    NSRange indexOfString= [text rangeOfString:@"timeStampRight"];
    if (indexOfString.length >0)
    {
        return;
    }
    ChatMessageViewController *msg = [[ChatMessageViewController alloc] initMessageWithText:text type:type];
    [msg.view setTag:tag];
    [self addChatMessageToScroll:msg];
}



- (void)addChatMessageToScroll:(ChatMessageViewController*)newMessage
{
    
    [self.contentScroll addSubview:newMessage.view];
    if (newMessage.messageType == kMessageTypeClient) {
        newMessage.view.frame = CGRectMake(self.view.frame.size.width - newMessage.view.frame.size.width - kMessageMargin, lastBubbleY, newMessage.view.frame.size.width, newMessage.view.frame.size.height);
    }
    else{
        newMessage.view.frame = CGRectMake(kMessageMargin, lastBubbleY, newMessage.view.frame.size.width, newMessage.view.frame.size.height);
    }
    
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tappedOnBubble:)];
    tap.numberOfTapsRequired = 1;
    [newMessage.view addGestureRecognizer:tap];
    
    
    lastBubbleY += newMessage.view.frame.size.height + kMessageToMessage;
    
    lastBubbleFrame = newMessage.view.frame;
    
    
    if (lastBubbleY> self.contentScroll.contentSize.height) {
        CGSize size = self.contentScroll.contentSize;
        size.height+= (lastBubbleY - size.height) + 20 ;
        self.contentScroll.contentSize = size;
        
        CGPoint bottomOffset = CGPointMake(0, self.contentScroll.contentSize.height - self.contentScroll.bounds.size.height);
        [self.contentScroll setContentOffset:bottomOffset animated:YES];
    }
    
    
}

-(void) reloadChat {
    for (int i = lastBubbleIndex; i<self.messages.count; i++) {
        NSDictionary* msgDic  = [self.messages objectAtIndex:i];
        ChatMessage* message = [[ChatMessage alloc] initWithDictionary:msgDic];
        [self initChatMessageBubble:message tag:i];
    }
    lastBubbleIndex = self.messages.count;
}

-(void) tappedOnBubble:(UITapGestureRecognizer*)sender{
    NSInteger tag = [sender.view tag];
    ChatMessage* message = [[ChatMessage alloc] initWithDictionary :[self.messages objectAtIndex:tag]];
    if (message.isHtml) {
        NSArray* arr  = [message.text componentsSeparatedByString:@"'"];
        
        NSString* url = @"";
        if (arr.count>3) {
            url = [arr objectAtIndex:3];
        }
        
        if (url.length>0) {
            webUrl = url;
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:webUrl]];

        //    [self performSegueWithIdentifier:@"goToWeb" sender:self];
        }
    }
}

- (IBAction)sendMsg:(id)sender {
    
    if (self.messageTextView.text.length<=0) {
        return;
    }
    
    [[NetworkManager shared] sendMessageWithSuccessBlock:self.messageTextView.text success:^(AFHTTPRequestOperation *operation , id data) {
        [self finishSendingMessageAnimated:YES];
    } failure:^(AFHTTPRequestOperation *operation, id data) {
        Error* error = data;
        [self showAlert:error.title];
        [self.messageTextView resignFirstResponder];
    }];
}

- (IBAction)backAct:(id)sender {
}

#pragma -mark CALL CONTENT

- (void)setGetCallContentTimer
{
    if (callStatusTimer) {
        [callStatusTimer invalidate];
    }
    callStatusTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(getCallContent) userInfo:nil repeats:YES];
}

- (void)getCallContent
{
    [[NetworkManager shared] getCallContentWithSuccess:YES success:^(AFHTTPRequestOperation *operation,id data) {
        
        CallContent *callContent  = data;
        
        switch (callContent.callStatus) {
            case WizSupportCallStatusActive:
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                
                if(callContent.isVideo>0 && !goToVideo){
                    goToVideo = YES;
                    [callStatusTimer invalidate];
                    [self performSegueWithIdentifier:@"startVideoChat" sender:self];
                }
                else{
                    if(callContent.isSnapper>0 && !goToSnapper){
                        goToSnapper = YES;
                        [callStatusTimer invalidate];
                        webUrl = @"";
                        [self performSegueWithIdentifier:@"goToWeb" sender:self];
                    }
                }
            }
                
                break;
            case WizSupportCallStatusClosed:
            {
                webUrl = @"";
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self performSegueWithIdentifier:@"goToWeb" sender:self];
            }
                
                break;
            case WizSupportCallStatusWaiting:
                return ;
                
            default:
                break;
        }
        
        self.messages = callContent.chatMessages;
        [self reloadChat];
        
    } failure:^(AFHTTPRequestOperation *operation, id data) {
        Error* error = data;
        [self showAlert:error.title];
        [callStatusTimer invalidate];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}



-(void) showAlert:(NSString*) msg {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self setGetCallContentTimer];
        [self.messageTextView becomeFirstResponder];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}


#pragma -mark UITextFieldDelegate

-(void) keyboardHidesBubbles{
    NSLog(@"%.2f",self.view.frame.size.height - lastBubbleFrame.origin.y - lastBubbleFrame.size.height - 64);
    if (self.view.frame.size.height - lastBubbleFrame.origin.y - lastBubbleFrame.size.height - 64< keyboardFrame.size.height) {
        UIEdgeInsets contentInsets;
        contentInsets = UIEdgeInsetsMake(0.0, 0.0, (keyboardFrame.size.height), 0.0);
        self.contentScroll.contentInset = contentInsets;
        self.contentScroll.scrollIndicatorInsets = contentInsets;
        
        CGPoint bottomOffset = CGPointMake(0, self.contentScroll.contentSize.height - self.contentScroll.bounds.size.height);
        [self.contentScroll setContentOffset:bottomOffset animated:YES];
    }

}


-(void)textViewDidBeginEditing:(UITextView *)textView{

}

-(void)textViewDidEndEditing:(UITextView *)textView{

}

-(BOOL)textFieldShouldReturn:(UITextField*)textField
{
    return YES;
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSLog(@"keyboard show");
    
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    keyboardFrame = [keyboardFrameBegin CGRectValue];
    self.sendBtn.enabled = (self.messageTextView.text.length>0);
    
    self.toolbarBottomLayout.constant = keyboardFrame.size.height;
    [UIView animateWithDuration:0.4
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:^(BOOL finished) {
                        
                     }];
     [self keyboardHidesBubbles];
}

-(void)keyboardWillHide:(NSNotification *)notification{
    NSLog(@"keyboard hide");
    
    self.sendBtn.enabled = (self.messageTextView.text.length>0);
    
    self.toolbarBottomLayout.constant =0;
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.contentScroll.contentInset = UIEdgeInsetsZero;
                         self.contentScroll.scrollIndicatorInsets = UIEdgeInsetsZero;
                         [self.view layoutIfNeeded];
                     }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    self.sendBtn.enabled = (textView.text.length>0);
    return YES;
}

- (void)finishSendingMessageAnimated:(BOOL)animated {
    
    self.messageTextView.text = nil;
    [self.messageTextView.undoManager removeAllActions];
    //[self.messageTextView resignFirstResponder];
}


-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"goToWeb"]) {
        WebChatViewController* webVCtrl = segue.destinationViewController;
        webVCtrl.url = webUrl;
    }
}

-(IBAction)backToChat:(UIStoryboardSegue *)segue {
    
}

@end
