//
//  ChatMessageViewController.m
//  Dojo
//
//  Created by Ohad Israeli on 10/08/15.
//  Copyright (c) 2015 Dojo. All rights reserved.
//

// No icon, no device name
#define kTextRightMargin        25
#define kTextLeftMarginNoIcon   25
#define kTextTopMargin          20
#define kTextBottomMargin       20

// With icon, no device name
#define kTextLeftMarginWithIcon 20

// With icon, with device name
#define kDeviceNameBottomMargin   13
#define kTextBottomMarginWithName 26
#define kRegularTextFont    [UIFont fontWithName:@"AvenirNext-Bold" size:18]
#define kDeviceNameColor [UIColor colorWithRed:247/255.f green:69/255.f blue:71/255.f alpha:1.0f]
#define kRegularTextColor       [UIColor colorWithRed:91/255.f green:86/255.f blue:84/255.f alpha:1.0f]
#define kDeviceNameFont     [UIFont fontWithName:@"AvenirNext-Bold" size:12]

#import "ChatMessageViewController.h"

@interface ChatMessageViewController ()
{
    CGFloat         originY;
    NSString        *currText;
    UIImage *bubbleImage;
}

@end

@implementation ChatMessageViewController

- (instancetype)initMessageWithText:(NSString *)text type:(ChatMessageType)type
{
    self = [super initWithNibName:@"ChatMessageViewController" bundle:nil];
    
    currText = text;
    _messageType = type;
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Set fonts and colors
    primaryTextLbl.font = kRegularTextFont;
    primaryTextLbl.textColor = kRegularTextColor;
    seconderyBttn.titleLabel.font = kDeviceNameFont;
    [seconderyBttn setTitleColor:kDeviceNameColor forState:UIControlStateNormal];
    
    // Ignore view from resizing its subviews
    self.view.autoresizesSubviews = NO;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated
{
    primaryTextLbl.text = currText;
    
    // Define strechable for bubble image
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(20, 20, 20, 20);
    
    switch (_messageType)
    {
        case kMessageTypeOper:{
            iconImageView.image = [UIImage imageNamed:@"agent"];
            bubbleImage = [[UIImage imageNamed:@"operMessageBubble"] resizableImageWithCapInsets:edgeInsets];
        }
            break;
            
        case kMessageTypeClient:{
            iconImageView.image = [UIImage imageNamed:@"client"];
            bubbleImage = [[UIImage imageNamed:@"clientMessageBubble"] resizableImageWithCapInsets:edgeInsets];
        }
            break;
            
        default:
            break;
    }
    
    [self setMarginsForCell];
}


#pragma mark - Internal Methods

- (void)setMarginsForCell
{
    
    
    CGFloat maxWidth = self.view.frame.size.width - kTextRightMargin*2 - kTextLeftMarginNoIcon*2;
    
    CGRect labelRect = [primaryTextLbl.text
                        boundingRectWithSize:CGSizeMake(maxWidth, 0)
                        options:NSStringDrawingUsesLineFragmentOrigin
                        attributes:@{NSFontAttributeName : kRegularTextFont}
                        context:nil];
    
    // Set text label frame
    primaryTextLbl.frame = CGRectMake(kTextLeftMarginNoIcon, kTextTopMargin,
                                      labelRect.size.width, labelRect.size.height);
    
    // Set background (buuble) frame according to the text label frame
    backgroundImageView.frame = CGRectMake(backgroundImageView.frame.origin.x, 0, CGRectGetMaxX(primaryTextLbl.frame) + kTextRightMargin , CGRectGetMaxY(primaryTextLbl.frame) + kTextBottomMargin);
    backgroundImageView.image = bubbleImage;
    
    // Check the message type
    if (_messageType == kMessageTypeOper)
    {
        iconImageView.center = CGPointMake(CGRectGetMinX(backgroundImageView.frame) + 8,
                                           CGRectGetMaxY(backgroundImageView.frame) - 8);
        // If text, set the frame of the whole message accordint to the bubble
        self.view.frame =  CGRectMake(0, 0, CGRectGetMaxX(backgroundImageView.frame), CGRectGetMaxY(iconImageView.frame));
    }
    else
    {
        // If not, add an icon to the left-bottom bubble's corener
        
        
        self.view.frame =  CGRectMake(0, 0, CGRectGetMaxX(backgroundImageView.frame),
                                      CGRectGetMaxY(iconImageView.frame));
        
        CGRect frame = iconImageView.frame;
        frame.origin.x =self.view.frame.size.width - frame.size.width +8;
        frame.origin.y -= 8 ;
        iconImageView.frame = frame;
    }

}



@end
