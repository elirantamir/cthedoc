//
//  WebChatViewController.h
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebChatViewController : UIViewController<UIWebViewDelegate>


@property (strong,nonatomic) NSOperationQueue* queue;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) NSString* url;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn;

@end
