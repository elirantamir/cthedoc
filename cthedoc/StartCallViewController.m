//
//  StartCallViewController.m
//  cthedoc
//
//  Created by Miky Braun on 26/07/2016.
//  Copyright © 2016 cthedoc.expert. All rights reserved.
//

#import "StartCallViewController.h"
#import "NetworkManager.h"
#import "MBProgressHUD.h"
#import "Error.h"

@interface StartCallViewController ()


@property (weak, nonatomic) IBOutlet UIButton *buttonStartChat;

@end

@implementation StartCallViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTranslucent:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonStartChatDidClicked:(id)sender
{
    [self.buttonStartChat setUserInteractionEnabled:NO];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    [[NetworkManager shared] startNewChatWithSuccessBlock:^(AFHTTPRequestOperation *operation, id data)
    {
        [self.buttonStartChat setUserInteractionEnabled:YES];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        BOOL success = [data boolValue];
        if (success) {
                   [self performSegueWithIdentifier:@"startNewChat" sender:self];
        }
        else
        {
            NSString* msg=@"Error starting chat";
            if ([data isKindOfClass:[Error class]]) {
                Error* error = data;
                msg = error.title;
            }
            [self showErrorAlert:msg];
        }

        
    } failure:^(AFHTTPRequestOperation *operation, id data) {
        
        [self.buttonStartChat setUserInteractionEnabled:YES];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        Error* error = data;
        [self showErrorAlert:error.title];
    }];
}


-(void) showErrorAlert:(NSString*)msg{
    NSLog(@"Error: %@",msg);
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"Try Again" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self buttonStartChatDidClicked:nil];
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
    }]];
     [self presentViewController:alert animated:YES completion:nil];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}


@end
