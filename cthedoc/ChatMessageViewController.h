//
//  ChatMessageViewController.h
//  Dojo
//
//  Created by Ohad Israeli on 10/08/15.
//  Copyright (c) 2015 Dojo. All rights reserved.
//

#import <UIKit/UIKit.h>

enum ChatMessageType
{
    kMessageTypeClient,
    kMessageTypeOper
}; typedef enum ChatMessageType ChatMessageType;

@interface ChatMessageViewController : UIViewController
{
    IBOutlet UILabel     *primaryTextLbl;
    IBOutlet UIButton    *seconderyBttn;
    IBOutlet UIImageView *iconImageView;
    IBOutlet UIImageView *backgroundImageView;
}

@property (nonatomic, readonly) ChatMessageType messageType;

- (instancetype)initMessageWithText:(NSString*)text type:(ChatMessageType)type;

@end
