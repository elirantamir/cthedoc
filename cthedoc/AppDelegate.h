//
//  AppDelegate.h
//  cthedoc
//
//  Created by Miky Braun on 26/07/2016.
//  Copyright © 2016 cthedoc.expert. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

