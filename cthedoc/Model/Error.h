//
//  Error.h
//  BeinLeumi
//
//  Created by Yosi Tsafar on 11/30/14.
//  Copyright (c) 2014 Yossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Error : NSObject

@property (strong, nonatomic) NSNumber *statusCode;
@property (strong, nonatomic) NSString *title;

@end
