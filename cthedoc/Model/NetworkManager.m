//
//  NetworkManager.m
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import "NetworkManager.h"

#define kWebServiceBaseURL @"http://piraeus.wizsupport.com/mobileapi/api/mobile/"
#define kWebServicePathStartNewChat @"startnewchat"
#define kWebServicePathGetCallContent @"getCallContent"
#define kWebServicePathSendMessage @"SendMsg"

const NSString *kGUID = @"fdcc1792-4c7d-4fac-a052-5bd0c2c5b35f";

@implementation NetworkManager

+ (NetworkManager *)shared
{
    static NetworkManager* sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[NetworkManager alloc] init];
    });
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        _data = [[UserData alloc] init];
        _data.password = @"123456";
        _data.extID = @"123123";
        _data.name = @"client";
        _data.guid = kGUID;
    }
    return self;
}

- (void)webServiceBaseRequestWithPattern:(NSString*)pattern params:(NSDictionary*)params success:(requestHandler)success failure:(requestHandler)failure
{
    NSString *baseURL = kWebServiceBaseURL;
    NSURL *url = [NSURL URLWithString:[baseURL stringByAppendingString:pattern]];

    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
    [mutableRequest setHTTPMethod:@"POST"];
    [mutableRequest setValue:@"application/json; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    NSError *error = nil;
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:kNilOptions error:&error];
    [mutableRequest setHTTPBody:jsonData];
    
    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:mutableRequest];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        if (success) {
            success(operation, responseObject);
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (failure) {
            failure(operation, error);
        }
    }];
    [op start];
    
}

#pragma mark - WebService methods

- (void)startNewChatWithSuccessBlock :(requestHandler)success failure:(requestHandler)failure
{
    
    
//    create new call
//    ==========================
//    testAPI("startnewchat",{"profile": "1","password": "123456","subject": "","agent":"","extra":"123123","username":"guest name", "data" : ""})
//    returns : room id
    
    
    NSDictionary *params = @{ @"profile" : @"1",
                              @"password" : self.data.password,
                              @"subject" : @"",
                              @"agent" : @"",
                              @"extra" : self.data.guid,
                              @"username" : self.data.name ,
                              @"data" : @"",
                              @"lang" : @"gr"
                              };
    
    
    [self webServiceBaseRequestWithPattern:kWebServicePathStartNewChat params:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       
                                       if ([responseObject isKindOfClass:[NSDictionary class]])
                                       {
                                           NSDictionary *responseDict = responseObject;
                                           NSString *responseString = [responseDict objectForKey:@"Message"];
                                           
                                           if (![responseString myContainsString:@"Error"] && responseString.length > 10)
                                           {

                                               if (success) {
                                                   NSString *roomId = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                                                   self.data.roomID = roomId;
                                                   NSLog(@"room id:%@",roomId);
                                                   success(operation, @YES);
                                               }
                                           }
                                           else
                                           {
                                               Error *err = [[Error alloc] init];
                                               err.statusCode = [NSNumber numberWithLong:11];
                                               err.title = @"נציג לא זמין כרגע";
                                               
                                               if (failure) {
                                                   failure(operation, err);
                                               }
                                           }
                                       }
                                       
                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       
                                       Error *err = [[Error alloc] init];
                                       err.statusCode = [NSNumber numberWithLong:error.code];
                                       err.title = error.localizedDescription;
                                       
                                       if (failure) {
                                           failure(operation, err);
                                       }
                                   }];
}

- (void) sendMessageWithSuccessBlock:(NSString*)text success:(requestHandler)success failure:(requestHandler)failure
{
    //    send message
    //    ============
    //    testAPI("getCallStatus",{"room":"roomId","password":"123456", "text": "hello"})
    //
    //    return { "responseCode": 0, "Message": ""} (0- success , -1 - failed)


    NSDictionary *params = @{ @"room" : self.data.roomID,
                              @"password" : self.data.password,
                              @"text" : text
                            };
    
    
    
    [self webServiceBaseRequestWithPattern:kWebServicePathSendMessage params:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       
                                       if ([responseObject isKindOfClass:[NSDictionary class]])
                                       {
                                           if ([[responseObject objectForKey:@"Message"] boolValue])
                                           {
                                                success(operation,responseObject);
                                           }
                                           else
                                           {
                                               Error *err = [[Error alloc] init];
                                               err.statusCode = [NSNumber numberWithLong:12];
                                               err.title = @"אירעה שגיאה בשליחת ההודעה";
                                               
                                               if (failure) {
                                                   failure(operation, err);
                                               }
                                           }
                                       }
                                       else
                                       {
                                           Error *err = [[Error alloc] init];
                                           err.statusCode = [NSNumber numberWithLong:12];
                                           err.title = @"אירעה שגיאה בשליחת ההודעה";
                                           
                                           if (failure) {
                                               failure(operation, err);
                                           }
                                       }
                                       
                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       
                                       Error *err = [[Error alloc] init];
                                       err.statusCode = [NSNumber numberWithLong:error.code];
                                       err.title = error.localizedDescription;
                                       
                                       if (failure) {
                                           failure(operation, err);
                                       }
                                       
                                   }];
}

- (void) getCallContentWithSuccess:(BOOL)typing success:(requestHandler)success failure:(requestHandler)failure
{
    //
    //    get call content
    //    =====================
    //    testAPI("getCallContent",{"room":"roomId","password":"123456", "guestsIsTyping" : 0 (optimal)})
    //
    //  return
    // {"Chat": [],"OperIsTyping": 0,"LastUpdate": "21/1/2017 14:05:04","isVideo": "0","isSnapper": "0","endStatus": "0","CallStatus": 4 }

    
    NSDictionary *params = @{ @"room" : self.data.roomID,
                              @"password" : self.data.password,
                              @"guestsIsTyping" : @(typing)
                            };
    
    [self webServiceBaseRequestWithPattern:kWebServicePathGetCallContent params:params
                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                       
                                       if ([responseObject isKindOfClass:[NSDictionary class]])
                                       {
                                           CallContent* callContent = [[CallContent alloc] initWithDictionary:responseObject];
                                           if (callContent)
                                           {
                                               if (success) {
                                                   success(operation,callContent);
                                               }
                                           }
                                       }
                                        else

                                       {
                                           Error *err = [[Error alloc] init];
                                           err.title = @"אירעה שגיאה";
                                           
                                           if (failure) {
                                               failure(operation, err);
                                           }
                                       }
                                       
                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                       
                                       Error *err = [[Error alloc] init];
                                       err.statusCode = [NSNumber numberWithLong:error.code];
                                       err.title = error.localizedDescription;
                                       
                                       if (failure) {
                                           failure(operation, err);
                                       }
                                       
                                   }];
}




@end
