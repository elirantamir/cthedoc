//
//  CallContent.m
//  cthedoc
//
//  Created by Eliran Sharabi on 21/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import "CallContent.h"

@implementation CallContent


-(id) initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        _chatMessages = [dic objectForKey:@"Chat"];
        _operIsTyping = [[dic objectForKey:@"OperIsTyping"] boolValue];
        _lastUpdate = [dic objectForKey:@"LastUpdate"];
        _isVideo = [[dic objectForKey:@"isVideo"] integerValue];
        _isSnapper= [[dic objectForKey:@"isSnapper"] integerValue];
        _endStatus = [[dic objectForKey:@"endStatus"] integerValue];
        _callStatus = [[dic objectForKey:@"CallStatus"] integerValue];
    }
    return self;
}


@end
