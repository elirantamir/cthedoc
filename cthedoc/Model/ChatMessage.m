//
//  ChatMessage.m
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import "ChatMessage.h"

@implementation ChatMessage

-(id) initWithDictionary:(NSDictionary*)dic{
    self = [super init];
    if (self) {
        _text = [dic objectForKey:@"message"];
        _isOper = [[dic objectForKey:@"IsOper"] boolValue];
        NSString* date = [dic objectForKey:@"date"];
        NSString* time = [dic objectForKey:@"time"];
        _time = [NSString stringWithFormat:@"%@ %@",date ,time];
        _isHtml = ([_text containsString:@"button"] || [_text containsString:@"span"]);
        
    }
    return self;
}

@end
