//
//  NetworkManager.h
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "Error.h"
#import "UserData.h"
#import "NSString+ContainString.h"
#import "CallContent.h"

@interface NetworkManager : NSObject


@property (strong, nonatomic) UserData *data;

+ (NetworkManager *)shared;

typedef void(^requestHandler)(AFHTTPRequestOperation *operation, id data);

- (void)startNewChatWithSuccessBlock :(requestHandler)success failure:(requestHandler)failure;

- (void) sendMessageWithSuccessBlock:(NSString*)text success:(requestHandler)success failure:(requestHandler)failure;

- (void) getCallContentWithSuccess:(BOOL)typing success:(requestHandler)success failure:(requestHandler)failure;



@end
