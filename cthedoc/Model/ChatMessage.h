//
//  ChatMessage.h
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChatMessage : NSObject

@property (strong,nonatomic) NSString* text;
@property (strong,nonatomic) NSString* time;
@property  BOOL isOper;
@property  BOOL isHtml;

-(id) initWithDictionary:(NSDictionary*)dic;

@end
