//
//  CallContent.h
//  cthedoc
//
//  Created by Eliran Sharabi on 21/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,WizSupportCallStatus) {
    WizSupportCallStatusWaiting = 1,
    WizSupportCallStatusActive = 2,
    WizSupportCallStatusClosed = 4,
} ;



@interface CallContent : NSObject


@property (strong, nonatomic) NSArray *chatMessages;
@property  BOOL operIsTyping;
@property (strong, nonatomic) NSDate *lastUpdate;
@property  NSInteger isVideo;
@property  NSInteger isSnapper;
@property  NSInteger endStatus;
@property  WizSupportCallStatus callStatus;

-(id) initWithDictionary:(NSDictionary*)dic;

@end
