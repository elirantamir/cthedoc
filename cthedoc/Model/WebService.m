////
////  WebService.m
////  WizSupportDemo
////
////  Created by Yossi on 16/06/15.
////  Copyright (c) 2015 Yossi. All rights reserved.
//
//// "http://fibi.wizsupport.com/mobileapi"
//
//
//#import "WebService.h"
//
//#define kWebServiceBaseURL @"http://talk2doc.wizsupport.com/mobileapi"
//#define kWebServicePathStartNewChat @"/api/mobile/startnewchat"
//#define kWebServicePathGetCallStatus @"/api/mobile/getCallStatus"
//#define kWebServicePathGetCallContent @"/api/mobile/getCallContent"
//
//const NSString *kGUID = @"fdcc1792-4c7d-4fac-a052-5bd0c2c5b35f";
//
//@implementation WebService
//
//+ (WebService *)sharedService
//{
//    static WebService* sharedInstance = nil;
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        sharedInstance = [[WebService alloc] init];
//    });
//    return sharedInstance;
//}
//
//- (id)init
//{
//    self = [super init];
//    if (self)
//    {
//        _data = [[UserData alloc] init];
//        _data.password = @"123456";
//        _data.extID = @"123123"; // @"FIBI757";
//        _data.name = @"client";
//    }
//    return self;
//}
//
//- (void)webServiceBaseRequestWithPattern:(NSString*)pattern params:(NSDictionary*)params success:(webServiceBaseRequestSuccessBlock)success failure:(webServiceBaseRequestFailureBlock)failure
//{
//    NSString *baseURL = kWebServiceBaseURL;
//    NSURL *url = [NSURL URLWithString:[baseURL stringByAppendingString:pattern]];
//    NSLog(@"%@", url);
//    NSMutableURLRequest *mutableRequest = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10];
//    [mutableRequest setHTTPMethod:@"POST"];
//    [mutableRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    
//    
//    NSError *error = nil;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:NSJSONWritingPrettyPrinted error:&error];
//    NSString *string = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
//    string = [string stringByReplacingOccurrencesOfString:@"\n" withString:@""];
//    [mutableRequest setHTTPBody:[string dataUsingEncoding:NSUTF8StringEncoding]];
//    
//    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:mutableRequest];
//    op.responseSerializer = [AFJSONResponseSerializer serializer];
//    //op.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        
//        if (success) {
//            
//            //            NSDictionary *dic = responseObject;
//            //            NSString *json = [dic valueForKey:@"d"];
//            //            if (json.length != 0)
//            //            {
//            //                if([json isEqualToString:@"0"])
//            //                {
//            //                    [self showErrorAlert];
//            //                }
//            //
//            //
//            //
//            //            }
//            success(operation, responseObject);
//        }
//        
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//        
//        if (failure) {
//            failure(operation, error);
//        }
//        
//    }];
//    [op start];
//    
//}
//
//#pragma mark - WebService methods
//
//- (void)webServiceCreateNewChatWithSubject:(NSString*)subject agent:(NSString*)agent extra:(NSString*)extra success:(createNewChatSuccessBlock)success failure:(checkRecordFailureBlock)failure
//{
//    //    create new call
//    //    ==========================
//    //    testAPI("startnewchat",{"profile": "1","password": "123456","subject": "","agent":"","extra":"123123","username":"שם שלי"})
//    //    testAPI("startnewchat",{"profile": "1","password": "123456","subject": "","agent":"","extra":"123123","username":"guest name"})
//    //    returns : room id
//    //
//    //  Profile: should come from somewhere
//    //  Password: static (what amit gave me)
//    //  Subject: Under profile / sub-profile (for ex.)
//    //  Agent: ?
//    //  Extra: Some random parmeter that I want to send to the representive
//    //  Username: Client's name that i'm getting in login method
//    //
//    //
//    NSString *password = self.data.password;
//    NSString *name = self.data.name;
//    
//    
////    NSDictionary *params = @{ @"profile" : @"1", @"password" : @"123456" ,@"subject" : subject, @"agent" : agent, @"extra" : extra, @"username" : name ,@"token":password};
////    NSDictionary *params = @{ @"profile" : @"1", @"data" : kGUID ,@"subject" : subject, @"agent" : agent, @"extra" : extra, @"username" : name ,@"token":password};
//    NSDictionary *params = @{ @"profile" : @"1", @"password" : password, @"subject" : @"", @"agent" : @"", @"extra" : extra, @"username" : name , @"data" : @""};
//    [self webServiceBaseRequestWithPattern:kWebServicePathStartNewChat params:params
//                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                       
//                                       if ([responseObject isKindOfClass:[NSDictionary class]])
//                                       {
//                                           NSDictionary *responseDict = responseObject;
//                                           NSString *responseString = [responseDict objectForKey:@"Message"];
//                                           
//                                           //NSRange range = [self rangeOfString:other];
//                                           if (![responseString myContainsString:@"Error"] && responseString.length > 10)
//                                           {
//                                               //   @@yosi -- TODO: The representive is Offline, check when is Online.
//                                               //                   Also, find out what is 'profile', 'subject', 'agent', 'extra' and 'username'.
//                                               //
//                                               NSString *GUID = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                                               self.data.guid = GUID;
//                                               
//                                               BOOL created = YES;
//                                               if (success) {
//                                                   success(operation, created);
//                                               }
//                                           }
//                                           else
//                                           {
//                                               Error *err = [[Error alloc] init];
//                                               err.statusCode = [NSNumber numberWithLong:11];
//                                               err.title = @"נציג לא זמין כרגע";
//                                               
//                                               if (failure) {
//                                                   failure(operation, err);
//                                               }
//                                           }
//                                       }
//                                       
//                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                       
//                                       Error *err = [[Error alloc] init];
//                                       err.statusCode = [NSNumber numberWithLong:error.code];
//                                       err.title = error.localizedDescription;
//                                       
//                                       if (failure) {
//                                           failure(operation, err);
//                                       }
//                                       
//                                   }];
//}
//
////{
////    Message = "Error: Couldn't create the call.";
////    responseCode = "-1";
////}
//
//- (void)webServiceGetCallStatusWithSuccessBlock:(getCallStatusSuccessBlock)success failure:(checkRecordFailureBlock)failure
//{
//    //    get call status
//    //    ============
//    //    testAPI("getCallStatus",{"room":"GUID","password":"123456"})  (1 - waiting , 2 active , 4 - closed)
//    //
//    //
//    //  If the call is in on-hold status will be 1
//    //  If the representive will click on get call, the status will be 2.
//    //  After getting status 2, call getCallContent
//    //
//    NSString *GUID = self.data.guid;
//    NSString *password = self.data.password;
//    NSString *extId = self.data.extID;
//    
//    //    NSDictionary *params = @{ @"room" : GUID, @"password" : password ,@"extra" : [DataManager sharedManager].userData.extID.stringValue,@"token":[DataManager sharedManager].userData.password};
//    
//    
//    NSDictionary *params = @{ @"room" : GUID, @"password" : password };
//    [self webServiceBaseRequestWithPattern:kWebServicePathGetCallStatus params:params
//                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                       
//                                       NSLog(@"%@", [responseObject valueForKey:@"Message"]);
//                                       
//                                       if ([responseObject isKindOfClass:[NSDictionary class]])
//                                       {
//                                           NSString *message = [responseObject valueForKey:@"Message"];
//                                           NSString *responseString = [message stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
//                                           
//                                           if (responseString.length != 0)
//                                           {
//                                               NSNumber *callStatus = [NSNumber numberWithInt:responseString.intValue];
//                                               if (success) {
//                                                   success(operation, callStatus.intValue);
//                                               }
//                                           }
//                                           else
//                                           {
//                                               Error *err = [[Error alloc] init];
//                                               err.statusCode = [NSNumber numberWithLong:12];
//                                               err.title = @"אירעה שגיאה בהתחלת השיחה";
//                                               
//                                               if (failure) {
//                                                   failure(operation, err);
//                                               }
//                                           }
//                                       }
//                                       else
//                                       {
//                                           Error *err = [[Error alloc] init];
//                                           err.statusCode = [NSNumber numberWithLong:12];
//                                           err.title = @"אירעה שגיאה בהתחלת השיחה";
//                                           
//                                           if (failure) {
//                                               failure(operation, err);
//                                           }
//                                       }
//                                       
//                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                       
//                                       Error *err = [[Error alloc] init];
//                                       err.statusCode = [NSNumber numberWithLong:error.code];
//                                       err.title = error.localizedDescription;
//                                       
//                                       if (failure) {
//                                           failure(operation, err);
//                                       }
//                                       
//                                   }];
//}
//
//- (void)webServiceGetCallContentWithSuccess:(getCallContentSuccessBlock)success failure:(checkRecordFailureBlock)failure
//{
//    //  shimi@wizsupport.com
//    //
//    //    get call content (plus isVideo)
//    //    =====================
//    //    testAPI("getCallContent",{"room":"GUID","password":"123456"})
//    //
//    //  Can remove getCallStatus because the status is retrive in here also.
//    //
//    //  get the text chat
//    //  video chat with the guid number (add or l or b)
//    //
//    //    NSDictionary *params = @{ @"room" : [DataManager sharedManager].userData.guid,
//    //                              @"password" : @"",
//    //                              @"extra" : [DataManager sharedManager].userData.extID.stringValue,@"token":[DataManager sharedManager].userData.password };
//    
//    NSDictionary *params = @{ @"room" : self.data.guid,
//                              @"password" : self.data.password };
//    
//    [self webServiceBaseRequestWithPattern:kWebServicePathGetCallContent params:params
//                                   success:^(AFHTTPRequestOperation *operation, id responseObject) {
//                                       
//                                       NSString *isVideo = [responseObject valueForKey:@"isVideo"];
//                                       if (isVideo.intValue != 0)
//                                       {
//                                           //   @@yosi -- Show chat
//                                           //
//                                           //
//                                           BOOL showChat = YES;
//                                           if (success) {
//                                               success(operation, showChat);
//                                           }
//                                       }
//                                       else
//                                       {
//                                           BOOL showChat = NO;
//                                           if (success) {
//                                               success(operation, showChat);
//                                           }
//                                       }
//                                       
//                                   } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//                                       
//                                       Error *err = [[Error alloc] init];
//                                       err.statusCode = [NSNumber numberWithLong:error.code];
//                                       err.title = error.localizedDescription;
//                                       
//                                       if (failure) {
//                                           failure(operation, err);
//                                       }
//                                       
//                                   }];
//}
//
//#pragma mark - Other methods
//
////  @@yosi -- This is not the best solution the precent urls, but it's temporary.
////
////
//- (NSString *)URLEncodeStringFromString:(NSString *)string
//{
//    return [string stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//}
//
//
//@end
