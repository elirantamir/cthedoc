////
////  WebService.h
////  WizSupportDemo
////
////  Created by Yossi on 16/06/15.
////  Copyright (c) 2015 Yossi. All rights reserved.
////
//
//#import <Foundation/Foundation.h>
//#import "AFNetworking.h"
//#import "Error.h"
//#import "UserData.h"
//#import "NSString+ContainString.h"
//
//typedef enum {
//    WizSupportCallStatusWaiting = 1,
//    WizSupportCallStatusActive = 2,
//    WizSupportCallStatusClosed = 4,
//} WizSupportCallStatus;
//
//@interface WebService : NSObject
//
//@property (strong, nonatomic) UserData *data;
//+ (WebService *)sharedService;
//
//typedef void(^webServiceBaseRequestSuccessBlock)(AFHTTPRequestOperation *operation, id responseObject);
//typedef void(^webServiceBaseRequestFailureBlock)(AFHTTPRequestOperation *operation, NSError *error);
//- (void)webServiceBaseRequestWithPattern:(NSString*)pattern params:(NSDictionary*)params success:(webServiceBaseRequestSuccessBlock)success failure:(webServiceBaseRequestFailureBlock)failure;
//
//typedef void(^checkRecordFailureBlock)(AFHTTPRequestOperation *operation, Error *error);
//
//typedef void(^createNewChatSuccessBlock)(AFHTTPRequestOperation *operation, BOOL created);
//- (void)webServiceCreateNewChatWithSubject:(NSString*)subject agent:(NSString*)agent extra:(NSString*)extra success:(createNewChatSuccessBlock)success failure:(checkRecordFailureBlock)failure;
//
//typedef void(^getCallStatusSuccessBlock)(AFHTTPRequestOperation *operation, WizSupportCallStatus status);
//- (void)webServiceGetCallStatusWithSuccessBlock:(getCallStatusSuccessBlock)success failure:(checkRecordFailureBlock)failure;
//
//typedef void(^getCallContentSuccessBlock)(AFHTTPRequestOperation *operation, BOOL showChat);
//- (void)webServiceGetCallContentWithSuccess:(getCallContentSuccessBlock)success failure:(checkRecordFailureBlock)failure;
//
//@end
