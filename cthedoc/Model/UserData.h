//
//  UserData.h
//  BeinLeumi
//
//  Created by Yossi on 07/12/14.
//  Copyright (c) 2014 Yossi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

@property (strong, nonatomic) NSString *login;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *extID;
@property (strong, nonatomic) NSString *roomID;
@property (strong, nonatomic) NSString *idNumber;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) const  NSString *guid;

@end
