//
//  NewVideoChatViewController.m
//  cthedoc
//
//  Created by Eliran Sharabi on 16/01/2017.
//  Copyright © 2017 cthedoc.expert. All rights reserved.
//

#import "WebChatViewController.h"
#import "MBProgressHUD.h"
#import "NetworkManager.h"

#define mashovUrl  @"https://piraeus.wizsupport.com/chat/imashov.aspx?op=1&profile=1&info=%22+mashov+%22&lang=he&smc=1"
#define kSnapperChatWebServiceURL @"https://piraeus.wizsupport.com/chat/snapper/snapperview.aspx?room="

@interface WebChatViewController ()

@end

@implementation WebChatViewController
{
    NSTimer* callStatusTimer;
    BOOL goToVideo;
    BOOL goToSnapper;
    BOOL showMashov;
    BOOL filePreview;
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    

    
    showMashov = NO;
    goToVideo = NO;
    goToSnapper = NO;
    filePreview  = NO;
    
    self.queue = [[NSOperationQueue alloc] init];
    self.queue.maxConcurrentOperationCount = 1;

    if (self.url.length>0) {
        filePreview = YES;
        [self loadWebChat];
    }
    else{
        [self.backBtn setImage:[UIImage new]];
        [self.backBtn setTitle:@""];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self setGetCallContentTimer];
    
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [callStatusTimer invalidate];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadWebChat {
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString* webStringURL = [self.url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:webStringURL];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:request];
}

-(void) loadMashovWebPage{
    UIBarButtonItem* done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAct:)];
    [self.navigationItem setRightBarButtonItem:done animated:YES];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSString* webStringURL = [mashovUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL* url = [NSURL URLWithString:webStringURL];
    
    [self.queue addOperationWithBlock:^{
        NSData* data = [[NSData alloc] initWithContentsOfURL:url];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.webView loadData:data MIMEType:@"text/html" textEncodingName:@"charset=utf-8" baseURL:[NSURL URLWithString:@"https://piraeus.wizsupport.com"]];
        }];
    }];
}


#pragma -mark CALL CONTENT

- (void)setGetCallContentTimer
{
    if (callStatusTimer) {
        [callStatusTimer invalidate];
    }
    callStatusTimer = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(getCallContent) userInfo:nil repeats:YES];
}

- (void)getCallContent
{
    [[NetworkManager shared] getCallContentWithSuccess:YES success:^(AFHTTPRequestOperation *operation,id data) {
        
        CallContent *callContent  = data;
        NSLog(@"status recieved = snapper:%ld  video:%ld",(long)callContent.isSnapper,(long)callContent.isVideo);

        switch (callContent.callStatus) {
            case WizSupportCallStatusActive:
            {
                
                if(callContent.isVideo>0 && callContent.isSnapper==0 ){
                    if (!goToVideo) {
                        goToVideo = YES;
                        [MBProgressHUD hideHUDForView:self.view animated:YES];
                        [self performSegueWithIdentifier:@"backToVideo" sender:self];
                    }
                }
                else{
                    if(callContent.isSnapper>0  ){
                        if (!goToSnapper) {
                            goToSnapper = YES;
                            self.url = [kSnapperChatWebServiceURL stringByAppendingString:[NetworkManager shared].data.roomID];
                            [self loadWebChat];
                        }

                    }
                    else{
                        if (!filePreview) {
                               [self performSegueWithIdentifier:@"backToChat" sender:self];
                        }
                     
                    }
                }
            }
                
                break;
            case WizSupportCallStatusClosed:
            {
                if (!showMashov) {
                    showMashov = YES;
                    [self loadMashovWebPage];
                }
                [callStatusTimer invalidate];
            }
                
                break;
            case WizSupportCallStatusWaiting:
                return ;
                
            default:
                break;
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, id data) {
        Error* error = data;
        [self showAlert:error.title];
        [callStatusTimer invalidate];
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}



-(void) showAlert:(NSString*) msg {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"" message:msg preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alert dismissViewControllerAnimated:YES completion:nil];
        [self setGetCallContentTimer];
    }]];
    [self presentViewController:alert animated:YES completion:nil];
}


- (IBAction)doneAct:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backAct:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma -mark UIWebViewDelegate

-(void) webViewDidStartLoad:(UIWebView *)webView{
}

-(void) webViewDidFinishLoad:(UIWebView *)webView{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
